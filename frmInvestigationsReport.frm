VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmInvestigationsReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Investigations Report"
   ClientHeight    =   8835
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13125
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8835
   ScaleWidth      =   13125
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   1440
      TabIndex        =   15
      Top             =   1080
      Width           =   4935
      Begin VB.OptionButton optCash 
         Caption         =   "&Cash"
         Height          =   240
         Left            =   1320
         TabIndex        =   19
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton optCredit 
         Caption         =   "C&redit"
         Height          =   240
         Left            =   2520
         TabIndex        =   18
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton optWard 
         Caption         =   "&Ward"
         Height          =   240
         Left            =   3720
         TabIndex        =   17
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton optAll 
         Caption         =   "&All"
         Height          =   240
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin btButtonEx.ButtonEx btnCancel 
      Height          =   495
      Left            =   11760
      TabIndex        =   7
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid gridIx 
      Height          =   5895
      Left            =   120
      TabIndex        =   6
      Top             =   2280
      Width           =   12855
      _ExtentX        =   22675
      _ExtentY        =   10398
      _Version        =   393216
   End
   Begin MSDataListLib.DataCombo cmbUser 
      Height          =   360
      Left            =   8040
      TabIndex        =   5
      Top             =   240
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1440
      TabIndex        =   3
      Top             =   240
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy MMMM dd"
      Format          =   144179203
      CurrentDate     =   40447
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   1440
      TabIndex        =   4
      Top             =   720
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy MMMM dd"
      Format          =   144179203
      CurrentDate     =   40447
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   1440
      TabIndex        =   9
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnProcess 
      Height          =   375
      Left            =   120
      TabIndex        =   10
      Top             =   1800
      Width           =   12855
      _ExtentX        =   22675
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "P&rocess"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmBHT 
      Height          =   360
      Left            =   8040
      TabIndex        =   11
      Top             =   720
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbCompany 
      Height          =   360
      Left            =   8040
      TabIndex        =   13
      Top             =   1200
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Bill Type"
      Height          =   240
      Left            =   240
      TabIndex        =   20
      Top             =   1200
      Width           =   720
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      Caption         =   "Company"
      Height          =   240
      Left            =   6840
      TabIndex        =   14
      Top             =   1200
      Width           =   795
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "BHT"
      Height          =   240
      Left            =   6840
      TabIndex        =   12
      Top             =   720
      Width           =   345
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "User"
      Height          =   240
      Left            =   6840
      TabIndex        =   2
      Top             =   240
      Width           =   390
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "From"
      Height          =   240
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   450
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "To"
      Height          =   240
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   225
   End
End
Attribute VB_Name = "frmInvestigationsReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temTopic As String
    Dim temSubTopic As String
    Dim temSelect As String
    Dim temFrom As String
    Dim temWhere As String
    Dim temOrderBy As String
    Dim temSQL As String
    Dim setDF As New cSetDfltPrinter
    
    
Private Sub SaveSettings()
    Call SaveCommonSettings(Me)
End Sub

Private Sub GetSettings()
    dtpFrom.Value = Date
    dtpTo.Value = Date
    Call GetCommonSettings(Me)
End Sub

Private Sub btnExcel_Click()
    
    GridToExcel gridIx, temTopic, temSubTopic
End Sub

Private Sub btnPrint_Click()
    Dim setDF As New cSetDfltPrinter
    setDF.SetPrinterAsDefault ""
    
    Dim MyPrinter As Printer
    
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = ReportPrinterName Then
            Set Printer = MyPrinter
        End If
    Next
    
    DoEvents
    
    
    setDF.SetPrinterAsDefault ReportPrinterName
    
    Dim myPR As PrintReport
    GetPrintDefaults myPR
    GridPrint gridIx, myPR, temTopic, temSubTopic
End Sub

Private Sub SetSelection()
    If optAll.Value = True Then
        cmbCompany.Text = Empty
        cmBHT.Text = Empty
        cmbCompany.Visible = False
        cmBHT.Visible = False
    ElseIf optCash.Value = True Then
        cmbCompany.Text = Empty
        cmBHT.Text = Empty
        cmbCompany.Visible = False
        cmBHT.Visible = False
    ElseIf optCredit.Value = True Then
        cmbCompany.Text = Empty
        cmBHT.Text = Empty
        cmbCompany.Visible = True
        cmBHT.Visible = False
    ElseIf optWard.Value = True Then
        cmbCompany.Text = Empty
        cmBHT.Text = Empty
        cmbCompany.Visible = False
        cmBHT.Visible = True
    End If
End Sub

Private Sub btnProcess_Click()
    Dim D(4) As Integer
    Dim p(1) As Integer
    temTopic = "Investigations Report"
    If optAll.Value = True Then
        temTopic = temTopic & " - All Bills"
    ElseIf optCash.Value = True Then
        temTopic = temTopic & " - Cash Bills"
    ElseIf optCredit.Value = True Then
        temTopic = temTopic & " - Company Bills"
    ElseIf optWard.Value = True Then
        temTopic = temTopic & " - Inward Bills"
    End If
    
    If dtpFrom.Value = dtpTo.Value Then
        temSubTopic = "On " & Format(dtpFrom.Value, "dd MMMM yyyy")
    Else
        temSubTopic = "From " & Format(dtpFrom.Value, "dd MMMM yyyy") & " To " & Format(dtpTo.Value, "dd MMMM yyyy")
    End If
    
    If IsNumeric(cmbUser.BoundText) = True Then
        temSubTopic = temSubTopic & " User - " & cmbUser.Text
    End If
    
    If IsNumeric(cmBHT.BoundText) = True Then
        temSubTopic = temSubTopic & " BHT - " & cmBHT.Text
    End If
    
    If IsNumeric(cmbCompany.BoundText) = True Then
        temSubTopic = temSubTopic & " Company - " & cmbCompany.Text
    End If
    
    temSelect = "SELECT tblPatientIxBill.PatientIxBillID AS [Bill No], "
    
    p(0) = 0
    ' p(1) = 1
    
    If dtpFrom.Value <> dtpTo.Value Then
        temSelect = temSelect & " format(tblPatientIxBill.Date, 'yyyy MMM dd') as [Bill Date], "
        D(0) = 3: D(1) = 4: D(2) = 5: D(3) = 6
    Else
        D(0) = 2: D(1) = 3: D(2) = 4: D(3) = 5
    End If
    
    If optCash.Value = True Then
        temSelect = temSelect & " tblPatient.Name as [Patient Name], "
        D(0) = D(0) + 1
        D(1) = D(1) + 1
        D(2) = D(2) + 1
        D(3) = D(3) + 1
    ElseIf optWard.Value = True And IsNumeric(cmBHT.BoundText) = True Then
    
    ElseIf optWard.Value = True And IsNumeric(cmBHT.BoundText) = False Then
        temSelect = temSelect & " tblBHT.BHT, "
        D(0) = D(0) + 1
        D(1) = D(1) + 1
        D(2) = D(2) + 1
        D(3) = D(3) + 1
        
    ElseIf optCredit.Value = True And IsNumeric(cmbCompany.BoundText) = True Then
        
    ElseIf optCredit.Value = True And IsNumeric(cmbCompany.BoundText) = False Then
        temSelect = temSelect & " tblCompany.Company, "
        D(0) = D(0) + 1
        D(1) = D(1) + 1
        D(2) = D(2) + 1
        D(3) = D(3) + 1
        
    ElseIf optAll.Value = True Then
        temSelect = temSelect & " tblPatientIxBill.BillType as [Bill Type], "
        D(0) = D(0) + 1
        D(1) = D(1) + 1
        D(2) = D(2) + 1
        D(3) = D(3) + 1
        
    End If
    
    temSelect = temSelect & " tblIx.Ix AS Investigation, Format(tblPatientIx.HospitalFee,'Fixed') AS [Hospital Fee], Format(tblPatientIx.StaffFee,'Fixed') AS [MLT Fee], Format(tblPatientIx.OtherFee,'Fixed') AS [Reagent Fee], Format(tblPatientIx.Value,'Fixed') AS Total "
    
    temFrom = "FROM ((((tblPatientIxBill LEFT JOIN tblPatientIx ON tblPatientIxBill.PatientIxBillID = tblPatientIx.PatientIxBillID) LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID) LEFT JOIN tblCompany ON tblPatientIxBill.CompanyID = tblCompany.CompanyID) LEFT JOIN tblBHT ON tblPatientIxBill.BHTID = tblBHT.BHTID) LEFT JOIN tblPatient ON tblPatientIxBill.PatientID = tblPatient.PatientID "
    temWhere = "WHERE (((tblPatientIxBill.Cancelled)=False) AND ((tblPatientIxBill.Date) Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "#) "
    If IsNumeric(cmbUser.BoundText) = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.UserID)=" & Val(cmbUser.BoundText) & ") "
        temSubTopic = temSubTopic & " User - " & cmbUser.Text
    End If
    
    
    If optCash.Value = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BillType)= 'Cash') "
    ElseIf optWard.Value = True And IsNumeric(cmBHT.BoundText) = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BillType)= 'Inward')  AND ((tblPatientIxBill.BHTID)= " & Val(cmBHT.BoundText) & ") "
    ElseIf optWard.Value = True And IsNumeric(cmBHT.BoundText) = False Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BillType)= 'Inward') "
    ElseIf optCredit.Value = True And IsNumeric(cmbCompany.BoundText) = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BillType)= 'Company')  AND ((tblPatientIxBill.CompanyID)= " & Val(cmbCompany.BoundText) & ") "
    ElseIf optCredit.Value = True And IsNumeric(cmbCompany.BoundText) = False Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BillType)= 'Company') "
    ElseIf optAll.Value = True Then
    
    End If
    
    temWhere = temWhere & " AND ((tblPatientIx.Deleted)=False)) "
    temOrderBy = "ORDER BY tblPatientIxBill.PatientIxBillID, tblPatientIx.PatientIxID "
    temSQL = temSelect & temFrom & temWhere & temOrderBy
    FillAnyGrid temSQL, gridIx, 1, D, p, True, 0, True, D(3)
    
    gridIx.Rows = gridIx.Rows + 1

End Sub

Private Sub cmbUser_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbUser.Text = Empty
    End If
End Sub

Private Sub cmbCompany_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbCompany.Text = Empty
    End If
End Sub



Private Sub Form_Load()
    Call FillCombos
    
    Call GetSettings
    Call SetSelection
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

Private Sub FillCombos()
    Dim Staff As New clsFillCombos
    Staff.FillSpecificField cmbUser, "Staff", "Name", False
    Dim Company As New clsFillCombos
    Company.FillAnyCombo cmbCompany, "Company", False
    Dim BHT As New clsFillCombos
    BHT.FillAnyCombo cmBHT, "BHT", False
End Sub

Private Sub optAll_Click()
    Call SetSelection
End Sub

Private Sub optCash_Click()
    Call SetSelection
End Sub

Private Sub optCredit_Click()
    Call SetSelection
End Sub

Private Sub optWard_Click()
    Call SetSelection
End Sub
