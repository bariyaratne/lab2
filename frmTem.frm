VERSION 5.00
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmTem 
   Caption         =   "Form1"
   ClientHeight    =   5595
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7800
   LinkTopic       =   "Form1"
   ScaleHeight     =   5595
   ScaleWidth      =   7800
   StartUpPosition =   3  'Windows Default
   Begin btButtonEx.ButtonEx ButtonEx1 
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Caption         =   "Capitalize All Ix"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Add Controls"
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   4800
      Width           =   1575
   End
End
Attribute VB_Name = "frmTem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ButtonEx1_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIx where Deleted = false"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !Ix = UCase(!Ix)
            .Update
            .MoveNext
        Wend
        .Close
    End With
End Sub

Private Sub Command1_Click()
    Dim rsForm As New ADODB.Recordset
    Dim rsControl As New ADODB.Recordset
    Dim MyForm As Form
    Dim FormID As Long
    Dim i As Integer
    Dim MyControl As Control
    Dim temText As String
    For Each MyForm In Forms
        FormID = GetFormID(MyForm.Name, MyForm.Caption)
        For Each MyControl In MyForm.Controls
            With rsForm
                If TypeOf MyControl Is SSTab Then
                    For i = 0 To MyControl.Tabs - 1
                        If .State = 1 Then .Close
                        temSql = "Select * from tblCOntrol where FormID = " & FormID & " AND COntrol = '" & MyControl.Name & "' AND ControlIndex = " & i
                        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
                        MyControl.Tab = i
                        If .RecordCount > 0 Then
                            !ControlText = GetControlText(MyControl)
                        Else
                            .AddNew
                            !FormID = FormID
                            !Control = MyControl.Name
                            !ControlType = GetControlType(MyControl)
                            !ControlText = GetControlText(MyControl)
                            !ControlIndex = i
                        End If
                        .Update
                        .Close
                    Next i
                Else
                    If .State = 1 Then .Close
                    temSql = "Select * from tblCOntrol where FormID = " & FormID & " AND COntrol = '" & MyControl.Name & "'"
                    .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
                    If .RecordCount > 0 Then
                        !ControlText = GetControlText(MyControl)
                    Else
                        .AddNew
                        !FormID = FormID
                        !Control = MyControl.Name
                        !ControlType = GetControlType(MyControl)
                        !ControlText = GetControlText(MyControl)
                    End If
                    .Update
                    .Close
                End If
            End With
        Next
    Next

End Sub
