VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Title"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varTitleID As Long
    Private varTitle As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblTitle Where TitleID = " & varTitleID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !Title = varTitle
        .Update
        varTitleID = !TitleID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblTitle WHERE TitleID = " & varTitleID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!TitleID) Then
               varTitleID = !TitleID
            End If
            If Not IsNull(!Title) Then
               varTitle = !Title
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varTitleID = 0
    varTitle = Empty
End Sub

Public Property Let TitleID(ByVal vTitleID As Long)
    Call clearData
    varTitleID = vTitleID
    Call loadData
End Property

Public Property Get TitleID() As Long
    TitleID = varTitleID
End Property

Public Property Let Title(ByVal vTitle As String)
    varTitle = vTitle
End Property

Public Property Get Title() As String
    Title = varTitle
End Property


