VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Ix"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Private varIxID As Long
    Private varIx As String
    Private varIxDisplay As String
    Private varIxCategoryID As Long
    Private varDeptID As Long
    Private varSampleID As Long
    Private varSpecimanID As Long
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedDate As Date
    Private varDeletedTime As Date
    Private varValue As Double
    Private varCost As Double
    Private varTubeID As Long
    Private varTubeVolume As Double
    Private varTubeUnitID As Long
    Private varBillComments As String
    Private varBillCommentsFontName As String
    Private varBillCommentsFontSize As Long
    Private varBillCommentsBold As Boolean
    Private varBillCommentsItalic As Boolean
    Private varComments As String
    Private varStaffCharge As Double
    Private varHospitalCharge As Double
    Private varOtherCharge As Double
    Private varMaterialCost As Double
    Private varExpenceCost As Double
    Private varOtherCost As Double
    Private varCharge As Double
    Private varPrintReportFormat As Boolean

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSql = "SELECT * FROM tblIx Where IxID = " & varIxID
        If .State = 1 Then .Close
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !Ix = varIx
        !IxDisplay = varIxDisplay
        !IxCategoryID = varIxCategoryID
        !DeptID = varDeptID
        !SampleID = varSampleID
        !SpecimanID = varSpecimanID
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !Value = varValue
        !Cost = varCost
        !TubeID = varTubeID
        !TubeVolume = varTubeVolume
        !TubeUnitID = varTubeUnitID
        !BillComments = varBillComments
        !BillCommentsFontName = varBillCommentsFontName
        !BillCommentsFontSize = varBillCommentsFontSize
        !BillCommentsBold = varBillCommentsBold
        !BillCommentsItalic = varBillCommentsItalic
        !Comments = varComments
        !StaffCharge = varStaffCharge
        !HospitalCharge = varHospitalCharge
        !OtherCharge = varOtherCharge
        !MaterialCost = varMaterialCost
        !ExpenceCost = varExpenceCost
        !OtherCost = varOtherCost
        !Charge = varCharge
        !PrintReportFormat = varPrintReportFormat
        .Update
        varIxID = !IxID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSql = "SELECT * FROM tblIx WHERE IxID = " & varIxID
        If .State = 1 Then .Close
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!IxID) Then
               varIxID = !IxID
            End If
            If Not IsNull(!Ix) Then
               varIx = !Ix
            End If
            If Not IsNull(!IxDisplay) Then
               varIxDisplay = !IxDisplay
            End If
            If Not IsNull(!IxCategoryID) Then
               varIxCategoryID = !IxCategoryID
            End If
            If Not IsNull(!DeptID) Then
               varDeptID = !DeptID
            End If
            If Not IsNull(!SampleID) Then
               varSampleID = !SampleID
            End If
            If Not IsNull(!SpecimanID) Then
               varSpecimanID = !SpecimanID
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!Value) Then
               varValue = !Value
            End If
            If Not IsNull(!Cost) Then
               varCost = !Cost
            End If
            If Not IsNull(!TubeID) Then
               varTubeID = !TubeID
            End If
            If Not IsNull(!TubeVolume) Then
               varTubeVolume = !TubeVolume
            End If
            If Not IsNull(!TubeUnitID) Then
               varTubeUnitID = !TubeUnitID
            End If
            If Not IsNull(!BillComments) Then
               varBillComments = !BillComments
            End If
            If Not IsNull(!BillCommentsFontName) Then
               varBillCommentsFontName = !BillCommentsFontName
            End If
            If Not IsNull(!BillCommentsFontSize) Then
               varBillCommentsFontSize = !BillCommentsFontSize
            End If
            If Not IsNull(!BillCommentsBold) Then
               varBillCommentsBold = !BillCommentsBold
            End If
            If Not IsNull(!BillCommentsItalic) Then
               varBillCommentsItalic = !BillCommentsItalic
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
            If Not IsNull(!StaffCharge) Then
               varStaffCharge = !StaffCharge
            End If
            If Not IsNull(!HospitalCharge) Then
               varHospitalCharge = !HospitalCharge
            End If
            If Not IsNull(!OtherCharge) Then
               varOtherCharge = !OtherCharge
            End If
            If Not IsNull(!MaterialCost) Then
               varMaterialCost = !MaterialCost
            End If
            If Not IsNull(!ExpenceCost) Then
               varExpenceCost = !ExpenceCost
            End If
            If Not IsNull(!OtherCost) Then
               varOtherCost = !OtherCost
            End If
            If Not IsNull(!Charge) Then
               varCharge = !Charge
            End If
            If Not IsNull(!PrintReportFormat) Then
               varPrintReportFormat = !PrintReportFormat
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varIxID = 0
    varIx = Empty
    varIxDisplay = Empty
    varIxCategoryID = 0
    varDeptID = 0
    varSampleID = 0
    varSpecimanID = 0
    varDeleted = False
    varDeletedUserID = 0
    varDeletedDate = Empty
    varDeletedTime = Empty
    varValue = 0
    varCost = 0
    varTubeID = 0
    varTubeVolume = 0
    varTubeUnitID = 0
    varBillComments = Empty
    varBillCommentsFontName = Empty
    varBillCommentsFontSize = 0
    varBillCommentsBold = False
    varBillCommentsItalic = False
    varComments = Empty
    varStaffCharge = 0
    varHospitalCharge = 0
    varOtherCharge = 0
    varMaterialCost = 0
    varExpenceCost = 0
    varOtherCost = 0
    varCharge = 0
    varPrintReportFormat = False
End Sub

Public Property Let IxID(ByVal vIxID As Long)
    Call clearData
    varIxID = vIxID
    Call loadData
End Property

Public Property Get IxID() As Long
    IxID = varIxID
End Property

Public Property Let Ix(ByVal vIx As String)
    varIx = vIx
End Property

Public Property Get Ix() As String
    Ix = varIx
End Property

Public Property Let IxDisplay(ByVal vIxDisplay As String)
    varIxDisplay = vIxDisplay
End Property

Public Property Get IxDisplay() As String
    IxDisplay = varIxDisplay
End Property

Public Property Let IxCategoryID(ByVal vIxCategoryID As Long)
    varIxCategoryID = vIxCategoryID
End Property

Public Property Get IxCategoryID() As Long
    IxCategoryID = varIxCategoryID
End Property

Public Property Let DeptID(ByVal vDeptID As Long)
    varDeptID = vDeptID
End Property

Public Property Get DeptID() As Long
    DeptID = varDeptID
End Property

Public Property Let SampleID(ByVal vSampleID As Long)
    varSampleID = vSampleID
End Property

Public Property Get SampleID() As Long
    SampleID = varSampleID
End Property

Public Property Let SpecimanID(ByVal vSpecimanID As Long)
    varSpecimanID = vSpecimanID
End Property

Public Property Get SpecimanID() As Long
    SpecimanID = varSpecimanID
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedDate(ByVal vDeletedDate As Date)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate() As Date
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime As Date)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime() As Date
    DeletedTime = varDeletedTime
End Property

Public Property Let Value(ByVal vValue As Double)
    varValue = vValue
End Property

Public Property Get Value() As Double
    Value = varValue
End Property

Public Property Let Cost(ByVal vCost As Double)
    varCost = vCost
End Property

Public Property Get Cost() As Double
    Cost = varCost
End Property

Public Property Let TubeID(ByVal vTubeID As Long)
    varTubeID = vTubeID
End Property

Public Property Get TubeID() As Long
    TubeID = varTubeID
End Property

Public Property Let TubeVolume(ByVal vTubeVolume As Double)
    varTubeVolume = vTubeVolume
End Property

Public Property Get TubeVolume() As Double
    TubeVolume = varTubeVolume
End Property

Public Property Let TubeUnitID(ByVal vTubeUnitID As Long)
    varTubeUnitID = vTubeUnitID
End Property

Public Property Get TubeUnitID() As Long
    TubeUnitID = varTubeUnitID
End Property

Public Property Let BillComments(ByVal vBillComments As String)
    varBillComments = vBillComments
End Property

Public Property Get BillComments() As String
    BillComments = varBillComments
End Property

Public Property Let BillCommentsFontName(ByVal vBillCommentsFontName As String)
    varBillCommentsFontName = vBillCommentsFontName
End Property

Public Property Get BillCommentsFontName() As String
    BillCommentsFontName = varBillCommentsFontName
End Property

Public Property Let BillCommentsFontSize(ByVal vBillCommentsFontSize As Long)
    varBillCommentsFontSize = vBillCommentsFontSize
End Property

Public Property Get BillCommentsFontSize() As Long
    BillCommentsFontSize = varBillCommentsFontSize
End Property

Public Property Let BillCommentsBold(ByVal vBillCommentsBold As Boolean)
    varBillCommentsBold = vBillCommentsBold
End Property

Public Property Get BillCommentsBold() As Boolean
    BillCommentsBold = varBillCommentsBold
End Property

Public Property Let BillCommentsItalic(ByVal vBillCommentsItalic As Boolean)
    varBillCommentsItalic = vBillCommentsItalic
End Property

Public Property Get BillCommentsItalic() As Boolean
    BillCommentsItalic = varBillCommentsItalic
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property

Public Property Let StaffCharge(ByVal vStaffCharge As Double)
    varStaffCharge = vStaffCharge
End Property

Public Property Get StaffCharge() As Double
    StaffCharge = varStaffCharge
End Property

Public Property Let HospitalCharge(ByVal vHospitalCharge As Double)
    varHospitalCharge = vHospitalCharge
End Property

Public Property Get HospitalCharge() As Double
    HospitalCharge = varHospitalCharge
End Property

Public Property Let OtherCharge(ByVal vOtherCharge As Double)
    varOtherCharge = vOtherCharge
End Property

Public Property Get OtherCharge() As Double
    OtherCharge = varOtherCharge
End Property

Public Property Let MaterialCost(ByVal vMaterialCost As Double)
    varMaterialCost = vMaterialCost
End Property

Public Property Get MaterialCost() As Double
    MaterialCost = varMaterialCost
End Property

Public Property Let ExpenceCost(ByVal vExpenceCost As Double)
    varExpenceCost = vExpenceCost
End Property

Public Property Get ExpenceCost() As Double
    ExpenceCost = varExpenceCost
End Property

Public Property Let OtherCost(ByVal vOtherCost As Double)
    varOtherCost = vOtherCost
End Property

Public Property Get OtherCost() As Double
    OtherCost = varOtherCost
End Property

Public Property Let Charge(ByVal vCharge As Double)
    varCharge = vCharge
End Property

Public Property Get Charge() As Double
    Charge = varCharge
End Property

Public Property Let PrintReportFormat(ByVal vPrintReportFormat As Boolean)
    varPrintReportFormat = vPrintReportFormat
End Property

Public Property Get PrintReportFormat() As Boolean
    PrintReportFormat = varPrintReportFormat
End Property


