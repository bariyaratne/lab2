VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmIxFormat 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Investigation  Format"
   ClientHeight    =   10860
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13035
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10860
   ScaleWidth      =   13035
   Begin VB.TextBox txtIxID 
      Height          =   375
      Left            =   4680
      TabIndex        =   80
      Top             =   120
      Width           =   615
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   9495
      Left            =   120
      TabIndex        =   32
      Top             =   600
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   16748
      _Version        =   393216
      Tabs            =   2
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Selected"
      TabPicture(0)   =   "frmIxFormat.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "frameSelect"
      Tab(0).Control(1)=   "frameEdit"
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "All"
      TabPicture(1)   =   "frmIxFormat.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "btnHorAlign"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "btnVerticalAlign"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "btnWidth"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "btnHeight"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "btnAllColour"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "btnAllFont"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "Frame7"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).ControlCount=   7
      Begin VB.Frame frameEdit 
         Height          =   3375
         Left            =   -74880
         TabIndex        =   43
         Top             =   6000
         Width           =   4935
         Begin VB.TextBox txtY2 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   4080
            TabIndex        =   79
            Top             =   1320
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.Frame frameLabel 
            Height          =   735
            Left            =   120
            TabIndex        =   75
            Top             =   1920
            Width           =   4455
            Begin VB.TextBox txtLabel 
               Height          =   360
               Left            =   840
               TabIndex        =   77
               Top             =   240
               Width           =   3495
            End
            Begin VB.TextBox txtReportItemID 
               BeginProperty Font 
                  Name            =   "Tahoma"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Left            =   2880
               TabIndex        =   76
               Top             =   360
               Visible         =   0   'False
               Width           =   375
            End
            Begin VB.Label Label1 
               Caption         =   "Text"
               Height          =   375
               Left            =   120
               TabIndex        =   78
               Top             =   240
               Width           =   1695
            End
         End
         Begin VB.Frame Frame3 
            Caption         =   "Size"
            Height          =   1695
            Left            =   1560
            TabIndex        =   68
            Top             =   240
            Width           =   1335
            Begin btButtonEx.ButtonEx btnSizeUp 
               Height          =   375
               Left            =   480
               TabIndex        =   69
               Top             =   360
               Width           =   375
               _ExtentX        =   661
               _ExtentY        =   661
               Appearance      =   3
               Caption         =   "5"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Webdings"
                  Size            =   9.75
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx btnSizeDown 
               Height          =   375
               Left            =   480
               TabIndex        =   70
               Top             =   1080
               Width           =   375
               _ExtentX        =   661
               _ExtentY        =   661
               Appearance      =   3
               Caption         =   "5"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Webdings"
                  Size            =   9.75
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx btnSizeRight 
               Height          =   375
               Left            =   840
               TabIndex        =   71
               Top             =   720
               Width           =   375
               _ExtentX        =   661
               _ExtentY        =   661
               Appearance      =   3
               Caption         =   "3"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Webdings"
                  Size            =   9.75
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx btnSizeLeft 
               Height          =   375
               Left            =   120
               TabIndex        =   72
               Top             =   720
               Width           =   375
               _ExtentX        =   661
               _ExtentY        =   661
               Appearance      =   3
               Caption         =   "3"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Webdings"
                  Size            =   9.75
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Move"
            Height          =   1695
            Left            =   120
            TabIndex        =   63
            Top             =   240
            Width           =   1335
            Begin btButtonEx.ButtonEx btnMoveUp 
               Height          =   375
               Left            =   480
               TabIndex        =   64
               Top             =   360
               Width           =   375
               _ExtentX        =   661
               _ExtentY        =   661
               Appearance      =   3
               Caption         =   "5"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Webdings"
                  Size            =   9.75
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx btnMoveRight 
               Height          =   375
               Left            =   840
               TabIndex        =   65
               Top             =   720
               Width           =   375
               _ExtentX        =   661
               _ExtentY        =   661
               Appearance      =   3
               Caption         =   "4"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Webdings"
                  Size            =   9.75
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx btnMoveLeft 
               Height          =   375
               Left            =   120
               TabIndex        =   66
               Top             =   720
               Width           =   375
               _ExtentX        =   661
               _ExtentY        =   661
               Appearance      =   3
               Caption         =   "3"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Webdings"
                  Size            =   9.75
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx btnMoveDown 
               Height          =   375
               Left            =   480
               TabIndex        =   67
               Top             =   1080
               Width           =   375
               _ExtentX        =   661
               _ExtentY        =   661
               Appearance      =   3
               Caption         =   "6"
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Webdings"
                  Size            =   9.75
                  Charset         =   2
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.TextBox txtX1 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3000
            TabIndex        =   62
            Top             =   1320
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.TextBox txtX2 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3360
            TabIndex        =   61
            Top             =   1320
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.TextBox txtY1 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3720
            TabIndex        =   60
            Top             =   1320
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.TextBox txtRadius 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3000
            TabIndex        =   59
            Top             =   1680
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.TextBox txtText 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3360
            TabIndex        =   58
            Top             =   1680
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.CheckBox chkLabel 
            Height          =   240
            Left            =   3720
            TabIndex        =   57
            Top             =   1680
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.CheckBox chkValue 
            Height          =   240
            Left            =   3960
            TabIndex        =   56
            Top             =   1680
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.CheckBox chkLine 
            Height          =   240
            Left            =   3960
            TabIndex        =   55
            Top             =   1920
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.CheckBox chkRectangle 
            Height          =   240
            Left            =   4200
            TabIndex        =   54
            Top             =   1680
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.CheckBox chkCircle 
            Height          =   240
            Left            =   3720
            TabIndex        =   53
            Top             =   1920
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.TextBox txtForeColour 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3000
            TabIndex        =   52
            Top             =   2040
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.TextBox txtBackColour 
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   3360
            TabIndex        =   51
            Top             =   2040
            Visible         =   0   'False
            Width           =   375
         End
         Begin VB.CheckBox chkCalc 
            Height          =   240
            Left            =   4200
            TabIndex        =   49
            Top             =   1920
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.OptionButton optJustified 
            Caption         =   "Justified"
            Height          =   255
            Left            =   3000
            TabIndex        =   48
            Top             =   3000
            Width           =   1335
         End
         Begin VB.OptionButton optCenter 
            Caption         =   "Center"
            Height          =   255
            Left            =   3000
            TabIndex        =   47
            Top             =   2760
            Width           =   1215
         End
         Begin VB.OptionButton optRight 
            Caption         =   "Right"
            Height          =   255
            Left            =   1920
            TabIndex        =   46
            Top             =   3000
            Width           =   855
         End
         Begin VB.OptionButton optLeft 
            Caption         =   "Left"
            Height          =   255
            Left            =   1920
            TabIndex        =   45
            Top             =   2760
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.CheckBox chkMultiline 
            Caption         =   "Multiline"
            Height          =   240
            Left            =   120
            TabIndex        =   44
            Top             =   2760
            Width           =   1815
         End
         Begin btButtonEx.ButtonEx btnBackColour 
            Height          =   375
            Left            =   3120
            TabIndex        =   50
            Top             =   1320
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "Back Colour"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx btnFont 
            Height          =   375
            Left            =   3120
            TabIndex        =   73
            Top             =   360
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "Font"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx btnForeColour 
            Height          =   375
            Left            =   3120
            TabIndex        =   74
            Top             =   840
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "Fore Colour"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame frameSelect 
         Height          =   5655
         Left            =   -74880
         TabIndex        =   38
         Top             =   360
         Width           =   4935
         Begin MSFlexGridLib.MSFlexGrid GridItem 
            Height          =   4815
            Left            =   120
            TabIndex        =   39
            Top             =   240
            Width           =   4695
            _ExtentX        =   8281
            _ExtentY        =   8493
            _Version        =   393216
            FocusRect       =   0
            SelectionMode   =   1
         End
         Begin btButtonEx.ButtonEx btnAdd 
            Height          =   375
            Left            =   120
            TabIndex        =   40
            Top             =   5160
            Width           =   1095
            _ExtentX        =   1931
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "Add"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx btnDelete 
            Height          =   375
            Left            =   2520
            TabIndex        =   41
            Top             =   5160
            Width           =   1095
            _ExtentX        =   1931
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "Delete"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx btnEdit 
            Height          =   375
            Left            =   1320
            TabIndex        =   42
            Top             =   5160
            Width           =   1095
            _ExtentX        =   1931
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "Edit"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Move"
         Height          =   1695
         Left            =   240
         TabIndex        =   33
         Top             =   480
         Width           =   1335
         Begin btButtonEx.ButtonEx btnAllUp 
            Height          =   375
            Left            =   480
            TabIndex        =   34
            Top             =   360
            Width           =   375
            _ExtentX        =   661
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "5"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Webdings"
               Size            =   9.75
               Charset         =   2
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx btnAllRight 
            Height          =   375
            Left            =   840
            TabIndex        =   35
            Top             =   720
            Width           =   375
            _ExtentX        =   661
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "4"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Webdings"
               Size            =   9.75
               Charset         =   2
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx btnAllLeft 
            Height          =   375
            Left            =   120
            TabIndex        =   36
            Top             =   720
            Width           =   375
            _ExtentX        =   661
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "3"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Webdings"
               Size            =   9.75
               Charset         =   2
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx btnAllDown 
            Height          =   375
            Left            =   480
            TabIndex        =   37
            Top             =   1080
            Width           =   375
            _ExtentX        =   661
            _ExtentY        =   661
            Appearance      =   3
            Caption         =   "6"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Webdings"
               Size            =   9.75
               Charset         =   2
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
      End
      Begin btButtonEx.ButtonEx btnAllFont 
         Height          =   375
         Left            =   1680
         TabIndex        =   82
         Top             =   600
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Font"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnAllColour 
         Height          =   375
         Left            =   1680
         TabIndex        =   83
         Top             =   1080
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Fore Colour"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnHeight 
         Height          =   375
         Left            =   240
         TabIndex        =   84
         Top             =   2280
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Fixed Height"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnWidth 
         Height          =   375
         Left            =   240
         TabIndex        =   85
         Top             =   2760
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Fixed Width"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnVerticalAlign 
         Height          =   375
         Left            =   1680
         TabIndex        =   86
         Top             =   2280
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Vertical Align"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx btnHorAlign 
         Height          =   375
         Left            =   1680
         TabIndex        =   87
         Top             =   2760
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Horizontal Align"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame6 
      Height          =   735
      Left            =   120
      TabIndex        =   29
      Top             =   10080
      Width           =   5175
      Begin btButtonEx.ButtonEx btnCopy 
         Height          =   375
         Left            =   3960
         TabIndex        =   30
         Top             =   240
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Copy"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo cmbCopyIx 
         Height          =   360
         Left            =   120
         TabIndex        =   31
         Top             =   240
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
   End
   Begin VB.TextBox txtRow 
      Height          =   360
      Left            =   240
      TabIndex        =   28
      Top             =   5280
      Visible         =   0   'False
      Width           =   855
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5280
      Top             =   4440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame5 
      Height          =   735
      Left            =   5160
      TabIndex        =   19
      Top             =   8040
      Visible         =   0   'False
      Width           =   4095
      Begin VB.TextBox txtItemForeColour 
         Height          =   330
         Left            =   3360
         TabIndex        =   27
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox txtItemBackColour 
         Height          =   360
         Left            =   2640
         TabIndex        =   26
         Top             =   240
         Width           =   615
      End
      Begin VB.CheckBox chkFontUnderline 
         Caption         =   "Check1"
         Height          =   255
         Left            =   2400
         TabIndex        =   25
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkFontStrikeThrough 
         Caption         =   "Check1"
         Height          =   255
         Left            =   2160
         TabIndex        =   24
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkFontItalic 
         Caption         =   "Check1"
         Height          =   255
         Left            =   1920
         TabIndex        =   23
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkFontBold 
         Caption         =   "Check1"
         Height          =   255
         Left            =   1680
         TabIndex        =   22
         Top             =   240
         Width           =   255
      End
      Begin VB.TextBox txtFontSize 
         Height          =   330
         Left            =   840
         TabIndex        =   21
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox txtFontName 
         Height          =   360
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame Frame4 
      Height          =   615
      Left            =   5160
      TabIndex        =   8
      Top             =   8160
      Visible         =   0   'False
      Width           =   2775
      Begin VB.CheckBox chkTextFontUnderline 
         Caption         =   "Check1"
         Height          =   255
         Left            =   2400
         TabIndex        =   14
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkTextFontStrikeThrough 
         Caption         =   "Check1"
         Height          =   255
         Left            =   2160
         TabIndex        =   13
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkTextFontItalic 
         Caption         =   "Check1"
         Height          =   255
         Left            =   1920
         TabIndex        =   12
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkTextFontBold 
         Caption         =   "Check1"
         Height          =   255
         Left            =   1680
         TabIndex        =   11
         Top             =   240
         Width           =   255
      End
      Begin VB.TextBox txtTextFontSize 
         Height          =   330
         Left            =   840
         TabIndex        =   10
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox txtTextFontName 
         Height          =   360
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   5160
      TabIndex        =   1
      Top             =   8040
      Visible         =   0   'False
      Width           =   2775
      Begin VB.CheckBox chkLabelFontUnderline 
         Caption         =   "Check1"
         Height          =   255
         Left            =   2400
         TabIndex        =   7
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkLabelFontStrikeThrough 
         Caption         =   "Check1"
         Height          =   255
         Left            =   2160
         TabIndex        =   6
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkLabelFontItalic 
         Caption         =   "Check1"
         Height          =   255
         Left            =   1920
         TabIndex        =   5
         Top             =   240
         Width           =   255
      End
      Begin VB.CheckBox chkLabelFontBold 
         Caption         =   "Check1"
         Height          =   255
         Left            =   1680
         TabIndex        =   4
         Top             =   240
         Width           =   255
      End
      Begin VB.TextBox txtLabelFontSize 
         Height          =   330
         Left            =   840
         TabIndex        =   3
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox txtLabelFontName 
         Height          =   360
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   615
      End
   End
   Begin VB.PictureBox pbxItem 
      AutoRedraw      =   -1  'True
      Height          =   9135
      Left            =   5400
      ScaleHeight     =   9075
      ScaleWidth      =   7275
      TabIndex        =   0
      Top             =   240
      Width           =   7335
   End
   Begin btButtonEx.ButtonEx btnSave 
      Height          =   375
      Left            =   7200
      TabIndex        =   15
      Top             =   9480
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "Save"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnCancel 
      Height          =   375
      Left            =   8400
      TabIndex        =   16
      Top             =   9480
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "Cancel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   375
      Left            =   11760
      TabIndex        =   18
      Top             =   9960
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox pbxItem1 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   9135
      Left            =   5400
      ScaleHeight     =   9075
      ScaleWidth      =   7275
      TabIndex        =   17
      Top             =   240
      Visible         =   0   'False
      Width           =   7335
   End
   Begin MSDataListLib.DataCombo cmbIx 
      Height          =   360
      Left            =   120
      TabIndex        =   81
      Top             =   120
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   "DataCombo1"
   End
End
Attribute VB_Name = "frmIxFormat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim i As Integer
    Dim IncreaseValue As Double
    Dim rsReportItem As New ADODB.Recordset
    Public FormIxID As Long
    Dim selectedId As Long
    
    
Private Sub btnAdd_Click()
    If IsNumeric(cmbIx.BoundText) = False Then
        MsgBox "Please select an investigation"
        cmbIx.SetFocus
        Exit Sub
    Else
        FormIxID = Val(cmbIx.BoundText)
    End If
    frmAddIxItemWizard.Show 1
    Call FormatGrid
    Call FillGrid
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
End Sub

Private Sub btnAllColour_Click()
    CommonDialog1.ShowColor
    If CommonDialog1.CancelError = True Then Exit Sub
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
           !ForeColour = CommonDialog1.Color
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image

End Sub

Private Sub btnAllDown_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !Y1 = !Y1 + IncreaseValue
            !Y2 = !Y2 + IncreaseValue
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image

End Sub

Private Sub btnAllFont_Click()
    CommonDialog1.FontName = txtFontName.text
    CommonDialog1.FontSize = Val(txtFontSize.text)
    CommonDialog1.FontBold = chkFontBold.Value
    CommonDialog1.FontItalic = chkFontItalic.Value
    CommonDialog1.FontUnderline = chkFontUnderline.Value
    CommonDialog1.FontStrikethru = chkFontStrikeThrough.Value
    CommonDialog1.Flags = cdlCFBoth Or cdlCFEffects
    CommonDialog1.ShowFont
    
    If CommonDialog1.CancelError = True Then Exit Sub
    
    If CommonDialog1.FontName = "" Then Exit Sub
    
    If CommonDialog1.FontSize = 0 Then Exit Sub
    
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !FontName = CommonDialog1.FontName
            !FontSize = CommonDialog1.FontSize
            !FontBold = CommonDialog1.FontBold
            !FontItalic = CommonDialog1.FontItalic
            !FONTSTRIKETHROUGH = CommonDialog1.FontStrikethru
            !FontUnderline = CommonDialog1.FontUnderline
'            !BackColour = MyReportDefault.LabelBackColour
'            !BackColour = Val(txtItemBackColour.text)
 '           !ForeColour = CommonDialog1.font
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image

End Sub

Private Sub btnAllLeft_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !X1 = !X1 - IncreaseValue
            !X2 = !X2 - IncreaseValue
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image

End Sub

Private Sub btnAllRight_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !X1 = !X1 + IncreaseValue
            !X2 = !X2 + IncreaseValue
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image

End Sub

Private Sub btnAllUp_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !Y1 = !Y1 - IncreaseValue
            !Y2 = !Y2 - IncreaseValue
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
End Sub

Private Sub btnBackColour_Click()
    CommonDialog1.ShowColor
    txtItemBackColour.text = CommonDialog1.Color
End Sub

Private Sub btnCancel_Click()
    Call FormatGrid
    Call FillGrid
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem)
    Call SelectMode
End Sub



Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnCopy_Click()
    Dim tr As Integer
    Dim i As Integer
    If IsNumeric(cmbIx.BoundText) = False Then
        MsgBox "Please select a test to copy to"
        cmbIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(cmbIx.BoundText) = False Then
        MsgBox "Please select a test to copy from"
        cmbCopyIx.SetFocus
        Exit Sub
    End If
    tr = MsgBox(cmbIx.text & " will be replaced by items found in " & cmbCopyIx.text & ". Are you sure?", vbYesNo)
    If tr = vbNo Then Exit Sub
    Dim rsFrom As New ADODB.Recordset
    Dim rsTo As New ADODB.Recordset
    With rsTo
        If .State = 1 Then .Close
        temSql = "SELECT * from tblIxItem where IxID = " & Val(cmbIx.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !Deleted = True
            !DeletedUserID = UserID
            !DeletedDate = Date
            !DeletedTime = Time
            .Update
            .MoveNext
        Wend
        .Close
    End With
    With rsFrom
        If .State = 1 Then .Close
        temSql = "SELECT * from tblIxItem where IxID = " & Val(cmbCopyIx.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            If rsTo.State = 1 Then rsTo.Close
            temSql = "SELECT * from tblIxItem where IxItemID = 0 "
            rsTo.Open temSql, cnnLab, adOpenStatic, adLockOptimistic
            rsTo.AddNew
            For i = 1 To .Fields.Count - 1
                rsTo.Fields(i).Value = .Fields(i).Value
            Next
            !IxID = Val(cmbIx.BoundText)
            rsTo.Update
            rsTo.Close
            .MoveNext
        Wend
        .Close
    End With
    Call FormatGrid
    Call FillGrid
    cmbIx_Change
    cmbIx.SetFocus
End Sub

Private Sub btnDelete_Click()
    Dim temRow As Integer
    Dim TemId As Long
    TemId = MsgBox("Are you sure you want to delete?", vbYesNo)
    If TemId = vbNo Then Exit Sub
    
    With GridItem
        temRow = .Row
        TemId = Val(.TextMatrix(temRow, 0))
        If TemId = 0 Then Exit Sub
    End With
    Dim rstemReportItems As New ADODB.Recordset
    With rstemReportItems
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem  where IxItemID = " & TemId
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedDate = Date
            !DeletedTime = Time
            !DeletedUserID = UserID
            .Update
        End If
        .Close
    End With
    Call FormatGrid
    Call FillGrid
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
End Sub

Private Sub btnEdit_Click()
    
    txtX1.text = "0.00"
    txtX2.text = "0.00"
    txtY1.text = "0.00"
    txtY2.text = "0.00"
    txtRadius.text = "0.00"
    
    chkLabel.Value = 0
    chkValue.Value = 0
    chkCalc.Value = 0
    chkLine.Value = 0
    chkCircle.Value = 0
    chkRectangle.Value = 0
    
    txtLabel.text = Empty
    
    txtFontName.text = Empty
    txtFontSize.text = Empty
    chkFontBold.Value = 0
    chkFontItalic.Value = 0
    chkFontStrikeThrough.Value = 0
    chkFontUnderline.Value = 0

    txtItemBackColour.text = Empty
    txtItemForeColour.text = Empty
    
    Dim temRow As Integer
    Dim TemId As Long
    With GridItem
        temRow = .Row
        TemId = Val(.TextMatrix(temRow, 0))
        If TemId = 0 Then Exit Sub
    End With
    
    Call EditMode
    
    pbxItem.Cls
    pbxItem1.Cls
    
    Call DrawGraphics(TemId, 0, pbxItem1)
    
    pbxItem.Picture = pbxItem1.Image
    
    Dim rstemReportItems As New ADODB.Recordset
    With rstemReportItems
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem  where IxItemID = " & TemId
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            txtX1.text = !X1
            txtX2.text = !X2
            txtY1.text = !Y1
            txtY2.text = !Y2
            txtRadius.text = !CircleRadius
            txtForeColour.text = !ForeColour
            txtBackColour.text = !BackColour
            txtReportItemID.text = !IxItemID
            
            If IsNull(!FontName) = False Then txtFontName.text = !FontName
            txtFontSize.text = !FontSize
            If !FontBold = True Then
                chkFontBold.Value = 1
            Else
                chkFontBold.Value = 0
            End If
            If !FontItalic = True Then
                chkFontItalic.Value = 1
            Else
                chkFontItalic.Value = 0
            End If
            If !FONTSTRIKETHROUGH = True Then
                chkFontStrikeThrough.Value = 1
            Else
                chkFontStrikeThrough.Value = 0
            End If
            If !FontUnderline = True Then
                chkFontUnderline.Value = 1
            Else
                chkFontUnderline.Value = 0
            End If
            
            txtItemBackColour.text = !BackColour
            txtItemForeColour.text = !ForeColour
            
            If !IsLabel = True Then
                txtLabel.text = !LabelText
                chkLabel.Value = 1
            Else
                txtLabel.text = !IxItem
            End If
            If !IsLine = True Then chkLine.Value = 1
            If !IsRectangle = True Then chkRectangle.Value = 1
            If !IsCircle = True Then chkCircle.Value = 1
            If !IsValue = True Then chkValue.Value = 1
            If !IsCalc = True Then chkCalc.Value = 1
            
            If !AllowMultiline = True Then
                chkMultiline.Value = 1
            Else
                chkMultiline.Value = 0
            End If
            
            Select Case !TextAlignment
                Case TextAlignment.CenterAlign: optCenter.Value = True
                Case TextAlignment.Justified: optJustified.Value = True
                Case TextAlignment.LeftAlign: optLeft.Value = True
                Case TextAlignment.RightAlign: optRight.Value = True
            End Select
            
            
        End If
        .Close
    End With
    Call DrawChanged
End Sub

Private Sub btnFont_Click()
    Dim MyDefaults As New clsReportDefault

            If chkFontBold.Value = 1 Then
            CommonDialog1.FontBold = True
        Else
            CommonDialog1.FontBold = False
        End If
        If chkFontItalic.Value = 1 Then
            CommonDialog1.FontItalic = True
        Else
            CommonDialog1.FontItalic = False
        End If
        
        If chkFontUnderline.Value = 1 Then
            CommonDialog1.FontUnderline = True
        Else
            CommonDialog1.FontUnderline = False
        End If
        If chkFontStrikeThrough.Value = 1 Then
            CommonDialog1.FontStrikethru = True
        Else
            CommonDialog1.FontStrikethru = False
        End If
    If Trim(txtFontName.text) = "" Then
        If chkValue.Value = 1 Then
            CommonDialog1.FontName = MyDefaults.TextFontName
        ElseIf chkLabel.Value = 1 Then
            CommonDialog1.FontName = MyDefaults.LabelFontName
        End If
    Else
        CommonDialog1.FontName = txtFontName.text
    End If
    
    If Val(txtFontSize.text) <= 0 Then
        If chkValue.Value = 1 Then
            CommonDialog1.FontSize = MyDefaults.TextFontSize
        ElseIf chkLabel.Value = 1 Then
            CommonDialog1.FontSize = MyDefaults.LabelFontSize
        End If
    Else
        CommonDialog1.FontSize = Val(txtFontSize.text)
    End If


    CommonDialog1.Flags = cdlCFBoth Or cdlCFEffects
    CommonDialog1.ShowFont
    If CommonDialog1.FontBold = True Then
        chkFontBold.Value = 1
    Else
        chkFontBold.Value = 0
    End If
    If CommonDialog1.FontItalic = True Then
        chkFontItalic.Value = 1
    Else
        chkFontItalic.Value = 0
    End If
    If CommonDialog1.FontUnderline = True Then
        chkFontUnderline.Value = 1
    Else
        chkFontUnderline.Value = 0
    End If
    If CommonDialog1.FontStrikethru = True Then
        chkFontStrikeThrough.Value = 1
    Else
        chkFontStrikeThrough.Value = 0
    End If
    txtFontName.text = CommonDialog1.FontName
    txtFontSize.text = CommonDialog1.FontSize
End Sub

Private Sub btnForeColour_Click()
    CommonDialog1.ShowColor
    txtItemForeColour.text = CommonDialog1.Color
End Sub

Private Sub btnHeight_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !Y2 = !Y1 + 0.02
            .Update
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
End Sub

Private Sub btnHorAlign_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !X1 = ((!X1 * 100) \ 2) / 50
            !X2 = ((!X2 * 100) \ 2) / 50
            .Update
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image

End Sub

Private Sub btnMoveDown_Click()
    pbxItem.Cls
    pbxItem.Picture = pbxItem1.Image
    txtY1.text = CDbl(txtY1.text) + IncreaseValue
    txtY2.text = CDbl(txtY2.text) + IncreaseValue
    DrawChanged

End Sub

Private Sub btnMoveLeft_Click()
    pbxItem.Cls
    pbxItem.Picture = pbxItem1.Image
    txtX1.text = CDbl(txtX1.text) - IncreaseValue
    txtX2.text = CDbl(txtX2.text) - IncreaseValue
    DrawChanged

End Sub

Private Sub btnMoveRight_Click()
    pbxItem.Cls
    pbxItem.Picture = pbxItem1.Image
    txtX1.text = CDbl(txtX1.text) + IncreaseValue
    txtX2.text = CDbl(txtX2.text) + IncreaseValue
    DrawChanged

End Sub

Private Sub btnMoveUp_Click()
    pbxItem.Cls
    pbxItem.Picture = pbxItem1.Image
    txtY1.text = CDbl(txtY1.text) - IncreaseValue
    txtY2.text = CDbl(txtY2.text) - IncreaseValue
    DrawChanged
End Sub

Private Sub DrawChanged()
    Dim TemX1 As Long
    Dim TemY1 As Long
    Dim TemX2 As Long
    Dim TemY2 As Long
    Dim temRadius As Long
    
    TemX1 = pbxItem.ScaleWidth * txtX1.text
    TemX2 = pbxItem.ScaleWidth * txtX2.text
    TemY1 = pbxItem.ScaleHeight * txtY1.text
    TemY2 = pbxItem.ScaleHeight * txtY2.text
    temRadius = pbxItem.ScaleWidth * txtRadius.text
    
    
    If chkCircle.Value = 1 Then
        pbxItem.Circle (TemX1, TemY1), temRadius
    ElseIf chkLabel.Value = 1 Or chkValue.Value = 1 Or chkCalc.Value = 1 Then
        pbxItem.Line (TemX1, TemY1)-(TemX2, TemY2), vbRed, BF
        If optCenter.Value = True Then
                pbxItem.CurrentX = ((TemX2 + TemX1) / 2) - (pbxItem.TextWidth(txtLabel.text) / 2)
        ElseIf optJustified.Value = True Then
                pbxItem.CurrentX = ((TemX2 + TemX1) / 2) - (pbxItem.TextWidth(txtLabel.text) / 2)
        ElseIf optLeft.Value = True Then
                pbxItem.CurrentX = TemX1
        ElseIf optRight.Value = True Then
                pbxItem.CurrentX = TemX2 - (pbxItem.TextWidth(txtLabel.text))
        End If
        pbxItem.CurrentY = TemY1
        pbxItem.Print txtLabel.text
    ElseIf chkLine.Value = 1 Then
        pbxItem.Line (TemX1, TemY1)-(TemX2, TemY2)
    ElseIf chkRectangle.Value = 1 Then
        pbxItem.Line (TemX1, TemY1)-(TemX2, TemY2), , B
    End If
    
End Sub

Private Sub btnSave_Click()
    Dim MyReportDefault As New clsReportDefault
    With rsReportItem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where IxItemID = " & Val(txtReportItemID.text)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
        
        !IxItem = txtLabel.text
        
        If chkCircle.Value = 1 Then
            !IsCircle = True
            !CircleRadius = Abs(CDbl(txtRadius.text))
        ElseIf chkLabel.Value = 1 Then
            !IsLabel = True
            !LabelText = txtLabel.text
        ElseIf chkLine.Value = 1 Then
            !IsLine = True
        ElseIf chkRectangle.Value = 1 Then
            !IsRectangle = True
        ElseIf chkValue.Value = 1 Then
            !IsValue = True
        ElseIf chkCalc.Value = True Then
            !IsCalc = True
        End If
        
        !X1 = CDbl(txtX1.text)
        !X2 = CDbl(txtX2.text)
        !Y1 = CDbl(txtY1.text)
        !Y2 = CDbl(txtY2.text)
        !CircleRadius = CDbl(txtRadius.text)
        
        If chkLabel.Value = 1 Then
            
            If Trim(txtFontName.text) = "" Then
                !FontName = MyReportDefault.LabelFontName
            Else
                !FontName = txtFontName.text
            End If
            If Val(txtFontSize.text) = 0 Then
                !FontSize = MyReportDefault.LabelFontSize
            Else
                !FontSize = Val(txtFontSize.text)
            End If
            !FontBold = chkFontBold.Value
            !FontItalic = chkFontItalic.Value
            !FONTSTRIKETHROUGH = chkFontStrikeThrough.Value
            !FontUnderline = chkFontUnderline.Value
            If Trim(txtItemBackColour.text) = "" Then
                !BackColour = MyReportDefault.LabelBackColour
            Else
                !BackColour = Val(txtItemBackColour.text)
            End If
            If Trim(txtItemForeColour.text) = "" Then
                !ForeColour = MyReportDefault.LabelForeColour
            Else
                !ForeColour = Val(txtItemForeColour.text)
            End If
         ElseIf chkValue.Value = 1 Or chkCalc.Value = 1 Then
            If Trim(txtFontName.text) = "" Then
                !FontName = MyReportDefault.TextFontName
            Else
                !FontName = txtFontName.text
            End If
            If Val(txtFontSize.text) = 0 Then
                !FontSize = MyReportDefault.TextFontSize
            Else
                !FontSize = Val(txtFontSize.text)
            End If
            !FontBold = chkFontBold.Value
            !FontItalic = chkFontItalic.Value
            !FONTSTRIKETHROUGH = chkFontStrikeThrough.Value
            !FontUnderline = chkFontUnderline.Value
            If Trim(txtItemBackColour.text) = "" Then
                !BackColour = MyReportDefault.TextBackColour
            Else
                !BackColour = Val(txtItemBackColour.text)
            End If
            If Trim(txtItemForeColour.text) = "" Then
                !ForeColour = MyReportDefault.TextForeColour
            Else
                !ForeColour = Val(txtItemForeColour.text)
            End If
        Else
            If Trim(txtItemBackColour.text) = "" Then
                !BackColour = vbBlack
            Else
                !BackColour = Val(txtItemBackColour.text)
            End If
            If Trim(txtItemForeColour.text) = "" Then
                !ForeColour = vbWhite
            Else
                !ForeColour = Val(txtItemForeColour.text)
            End If
        End If
        
        If chkMultiline.Value = 1 Then
            !AllowMultiline = True
        Else
            !AllowMultiline = False
        End If
        
        If optCenter.Value = True Then
            !TextAlignment = TextAlignment.CenterAlign
        ElseIf optJustified.Value = True Then
            !TextAlignment = TextAlignment.Justified
        ElseIf optLeft.Value = True Then
            !TextAlignment = TextAlignment.LeftAlign
        ElseIf optRight.Value = True Then
            !TextAlignment = TextAlignment.RightAlign
        End If
        
        .Update
        End If
        .Close
    End With
    Call FormatGrid
    Call FillGrid
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
    Call SelectMode
End Sub

Private Sub btnSizeDown_Click()
    pbxItem.Cls
    pbxItem.Picture = pbxItem1.Image
    txtY2.text = CDbl(txtY2.text) - IncreaseValue
    txtRadius.text = CDbl(txtRadius.text) - IncreaseValue
    DrawChanged
End Sub

Private Sub btnSizeLeft_Click()
    pbxItem.Cls
    pbxItem.Picture = pbxItem1.Image
    txtX1.text = CDbl(txtX1.text) - IncreaseValue
    txtRadius.text = CDbl(txtRadius.text) + IncreaseValue
    DrawChanged
End Sub

Private Sub btnSizeRight_Click()
    pbxItem.Cls
    pbxItem.Picture = pbxItem1.Image
    txtX2.text = CDbl(txtX2.text) - IncreaseValue
    txtRadius.text = CDbl(txtRadius.text) - IncreaseValue
    DrawChanged
End Sub

Private Sub btnSizeUp_Click()
    pbxItem.Cls
    pbxItem.Picture = pbxItem1.Image
    txtY1.text = CDbl(txtY1.text) - IncreaseValue
    txtRadius.text = CDbl(txtRadius.text) + IncreaseValue
    DrawChanged
End Sub

Private Sub FillCombos()
    Dim Ix As New clsFillCombos
    Ix.FillAnyCombo cmbIx, "Ix", True
    Dim copyIx As New clsFillCombos
    copyIx.FillAnyCombo cmbCopyIx, "Ix", True
End Sub


Private Sub ButtonEx1_Click()
    CommonDialog1.FontName = txtFontName.text
    CommonDialog1.FontSize = Val(txtFontSize.text)
    CommonDialog1.FontBold = chkFontBold.Value
    CommonDialog1.FontItalic = chkFontItalic.Value
    CommonDialog1.FontUnderline = chkFontUnderline.Value
    CommonDialog1.FontStrikethru = chkFontStrikeThrough.Value
    CommonDialog1.Flags = cdlCFBoth Or cdlCFEffects
    CommonDialog1.ShowFont
    
    If CommonDialog1.CancelError = True Then Exit Sub
    
    If CommonDialog1.FontName = "" Then Exit Sub
    
    If CommonDialog1.FontSize = 0 Then Exit Sub
    
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !FontName = CommonDialog1.FontName
            !FontSize = CommonDialog1.FontSize
            !FontBold = CommonDialog1.FontBold
            !FontItalic = CommonDialog1.FontItalic
            !FONTSTRIKETHROUGH = CommonDialog1.FontStrikethru
            !FontUnderline = CommonDialog1.FontUnderline
'            !BackColour = MyReportDefault.LabelBackColour
'            !BackColour = Val(txtItemBackColour.text)
 '           !ForeColour = CommonDialog1.font
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
End Sub

Private Sub ButtonEx2_Click()

End Sub

Private Sub btnVerticalAlign_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !Y1 = ((!Y1 * 100) \ 2) / 50
            !Y2 = ((!Y2 * 100) \ 2) / 50
            .Update
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
End Sub

Private Sub btnWidth_Click()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by IxItemID"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !X2 = !X1 + 0.3
            .Update
            .MoveNext
        Wend
        .Close
    End With
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
End Sub

Private Sub cmbIx_Change()
    FormIxID = Val(cmbIx.BoundText)
    Call FormatGrid
    Call FillGrid
    pbxItem.Cls
    pbxItem1.Cls
    Call DrawGraphics(0, 0, pbxItem1)
    pbxItem.Picture = pbxItem1.Image
    txtIxID.text = Val(cmbIx.BoundText)
End Sub

Private Sub Form_Load()
    IncreaseValue = 0.005
    
    txtX1.text = "0.00"
    txtX2.text = "0.00"
    txtY1.text = "0.00"
    txtY2.text = "0.00"
    txtRadius.text = "0.00"
    
    Call FillCombos
    Call SelectMode
    Call FormatGrid
    
End Sub

Private Sub FormatGrid()
    With GridItem
        .Clear
        .rows = 1
        .Cols = 2
        .ColWidth(0) = 0
        .ColWidth(1) = .Width - 150
    End With
End Sub

Private Sub SelectMode()

    frameSelect.Enabled = True
    
    btnSave.Enabled = False
    btnCancel.Enabled = False
    
    
    btnAdd.Enabled = True
    btnEdit.Enabled = True
    btnDelete.Enabled = True
    GridItem.Enabled = True
    
    btnMoveDown.Enabled = False
    btnMoveLeft.Enabled = False
    btnMoveRight.Enabled = False
    btnMoveUp.Enabled = False
    
    btnSizeDown.Enabled = False
    btnSizeLeft.Enabled = False
    btnSizeRight.Enabled = False
    btnSizeUp.Enabled = False
    
    btnFont.Enabled = False
    btnBackColour.Enabled = False
    btnForeColour.Enabled = False
    
    frameLabel.Visible = False

End Sub

Private Sub EditMode()
    frameSelect.Enabled = False
    
    btnSave.Enabled = True
    btnCancel.Enabled = True
    
    btnAdd.Enabled = False
    btnEdit.Enabled = False
    btnDelete.Enabled = False
    GridItem.Enabled = False
    
    btnMoveDown.Enabled = True
    btnMoveLeft.Enabled = True
    btnMoveRight.Enabled = True
    btnMoveUp.Enabled = True
    
    btnSizeDown.Enabled = True
    btnSizeLeft.Enabled = True
    btnSizeRight.Enabled = True
    btnSizeUp.Enabled = True
    
    btnFont.Enabled = True
    btnBackColour.Enabled = True
    btnForeColour.Enabled = True
    
    frameLabel.Visible = True
    
End Sub

Private Sub FillGrid()
    With rsReportItem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where Deleted = false And IxID = " & Val(cmbIx.BoundText) & " Order by isLabel, isValue, isCalc, y1, x1"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        FormatGrid
        If .RecordCount > 0 Then
            .MoveLast
            Dim LabelCount As Long
            .MoveFirst
            LabelCount = .RecordCount
            Dim i As Integer
            GridItem.rows = 1
            While .EOF = False
                GridItem.rows = GridItem.rows + 1
                GridItem.Row = GridItem.rows - 1
                GridItem.col = 0
                If !IsValue = True Then
                    GridItem.CellBackColor = RGB(255, 204, 51)
                ElseIf !IsLabel = True Then
                    GridItem.CellBackColor = RGB(153, 204, 51)
                ElseIf !IsCalc = True Then
                    GridItem.CellBackColor = RGB(53, 151, 53)
                End If
                GridItem.text = !IxItemID
                GridItem.col = 1
                GridItem.text = !IxItem
                If !IsValue = True Then
                    GridItem.CellBackColor = RGB(255, 204, 51)
                ElseIf !IsLabel = True Then
                    GridItem.CellBackColor = RGB(153, 204, 51)
                ElseIf !IsCalc = True Then
                    GridItem.CellBackColor = RGB(53, 151, 53)
                End If
                .MoveNext
            Wend
            
        End If
        .Close
    End With
    If Val(txtRow.text) <> 0 Then
        On Error Resume Next
        GridItem.topRow = Val(txtRow.text)
    End If
    locateInGrid
End Sub







Private Sub ReportGraphics(ExceptID As Long, OnlyID As Long, MyPicture As PictureBox)
    Dim rstemReportItems As New ADODB.Recordset
    Dim TemX1 As Long
    Dim TemY1 As Long
    Dim TemX2 As Long
    Dim TemY2 As Long
    Dim temRadius As Long
    
    
    With rstemReportItems
        If .State = 1 Then .Close
        If ExceptID <> 0 Then
            temSql = "Select * from tblReportItem  where Deleted = false And ReportItemID <> " & ExceptID
        ElseIf OnlyID <> 0 Then
            temSql = "Select * from tblReportItem  where ReportItemID = " & OnlyID
        Else
            temSql = "Select * from tblReportItem  where Deleted = false"
        End If
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
        
            TemX1 = MyPicture.ScaleWidth * !X1
            TemX2 = MyPicture.ScaleWidth * !X2
            TemY1 = MyPicture.ScaleHeight * !Y1
            TemY2 = MyPicture.ScaleHeight * !Y2
            
            If IsNull(!CircleRadius) = False Then
                temRadius = MyPicture.ScaleWidth * !CircleRadius
            End If
            If !IsLine = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour
            ElseIf !IsRectangle = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour, B
            ElseIf !IsCircle = True Then
                MyPicture.Circle (TemX1, TemY1), temRadius, !ForeColour
            ElseIf !IsLabel = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), vbYellow, BF
                Select Case !TextAlignment
                    Case TextAlignment.CenterAlign:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!LabelText) / 2)
                    Case TextAlignment.Justified:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!LabelText) / 2)
                    Case TextAlignment.LeftAlign:
                        MyPicture.CurrentX = TemX1
                    Case TextAlignment.RightAlign:
                        MyPicture.CurrentX = TemX2 - (MyPicture.TextWidth(!LabelText))
                End Select
                MyPicture.CurrentY = TemY1
                MyPicture.ForeColor = !ForeColour
                MyPicture.Print !LabelText
            ElseIf !IsText = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), vbYellow, BF
                Select Case !TextAlignment
                    Case TextAlignment.CenterAlign:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!TextText) / 2)
                    Case TextAlignment.Justified:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!TextText) / 2)
                    Case TextAlignment.LeftAlign:
                        MyPicture.CurrentX = TemX1
                    Case TextAlignment.RightAlign:
                        MyPicture.CurrentX = TemX2 - MyPicture.TextWidth(!TextText)
                End Select
                MyPicture.CurrentY = TemY1
                MyPicture.ForeColor = !ForeColour
                MyPicture.Print !TextText
            End If
            .MoveNext
        Wend
        .Close
    End With

End Sub










Private Sub DrawGraphics(ExceptID As Long, OnlyID As Long, MyPicture As PictureBox)
    
    Call ReportGraphics(0, 0, MyPicture)
    
    Dim rstemReportItems As New ADODB.Recordset
    Dim TemX1 As Long
    Dim TemY1 As Long
    Dim TemX2 As Long
    Dim TemY2 As Long
    Dim temRadius As Long
    
    
    With rstemReportItems
        If .State = 1 Then .Close
        If ExceptID <> 0 Then
            temSql = "Select * from tblIxItem  where Deleted = false And IxItemID <> " & ExceptID & " AND IxID = " & Val(cmbIx.BoundText)
        ElseIf OnlyID <> 0 Then
            temSql = "Select * from tblIxItem  where IxItemID = " & OnlyID
        Else
            temSql = "Select * from tblIxItem  where Deleted = false  AND IxID = " & Val(cmbIx.BoundText)
        End If
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
        
            TemX1 = MyPicture.ScaleWidth * !X1
            TemX2 = MyPicture.ScaleWidth * !X2
            TemY1 = MyPicture.ScaleHeight * !Y1
            TemY2 = MyPicture.ScaleHeight * !Y2
            
            If IsNull(!CircleRadius) = False Then
                temRadius = MyPicture.ScaleWidth * !CircleRadius
            End If
            If !IsLine = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour
            ElseIf !IsRectangle = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour, B
            ElseIf !IsCircle = True Then
                MyPicture.Circle (TemX1, TemY1), temRadius, !ForeColour
            ElseIf !IsLabel = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), RGB(153, 204, 51), BF
                Select Case !TextAlignment
                    Case TextAlignment.CenterAlign:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!IxItem) / 2)
                    Case TextAlignment.Justified:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!IxItem) / 2)
                    Case TextAlignment.LeftAlign:
                        MyPicture.CurrentX = TemX1
                    Case TextAlignment.RightAlign:
                        MyPicture.CurrentX = TemX2 - (MyPicture.TextWidth(!IxItem))
                End Select
                MyPicture.CurrentY = TemY1
                MyPicture.ForeColor = !ForeColour
                MyPicture.Print !IxItem
            ElseIf !IsValue = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), RGB(255, 204, 51), BF
                Select Case !TextAlignment
                    Case TextAlignment.CenterAlign:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!IxItem) / 2)
                    Case TextAlignment.Justified:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!IxItem) / 2)
                    Case TextAlignment.LeftAlign:
                        MyPicture.CurrentX = TemX1
                    Case TextAlignment.RightAlign:
                        MyPicture.CurrentX = TemX2 - (MyPicture.TextWidth(!IxItem))
                End Select
                MyPicture.CurrentY = TemY1
                MyPicture.ForeColor = !ForeColour
                MyPicture.Print !IxItem
            ElseIf !IsCalc = True Then
                MyPicture.Line (TemX1, TemY1)-(TemX2, TemY2), RGB(53, 151, 53), BF
                Select Case !TextAlignment
                    Case TextAlignment.CenterAlign:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!IxItem) / 2)
                    Case TextAlignment.Justified:
                        MyPicture.CurrentX = ((TemX2 + TemX1) / 2) - (MyPicture.TextWidth(!IxItem) / 2)
                    Case TextAlignment.LeftAlign:
                        MyPicture.CurrentX = TemX1
                    Case TextAlignment.RightAlign:
                        MyPicture.CurrentX = TemX2 - (MyPicture.TextWidth(!IxItem))
                End Select
                MyPicture.CurrentY = TemY1
                MyPicture.ForeColor = !ForeColour
                MyPicture.Print !IxItem
            End If
            .MoveNext
        Wend
        .Close
    End With

End Sub

Private Sub locateInGrid()
  '  On Error Resume Next
    Dim temRow As Long
    For temRow = 0 To GridItem.rows - 1
        If Val(GridItem.TextMatrix(temRow, 0)) = selectedId Then
            GridItem.Row = temRow
        End If
    Next
End Sub

Private Sub GridItem_Click()
    txtRow.text = GridItem.topRow
    selectedId = Val(GridItem.TextMatrix(GridItem.Row, 0))
End Sub

Private Sub txtIxID_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        cmbIx.BoundText = Val(txtIxID.text)
    End If
End Sub
