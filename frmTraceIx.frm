VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmTraceIx 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Investigation Lists"
   ClientHeight    =   7800
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12900
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7800
   ScaleWidth      =   12900
   Begin MSFlexGridLib.MSFlexGrid gridIx 
      Height          =   4575
      Left            =   360
      TabIndex        =   7
      Top             =   2160
      Width           =   12135
      _ExtentX        =   21405
      _ExtentY        =   8070
      _Version        =   393216
      WordWrap        =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin VB.OptionButton optByID 
      Caption         =   "By Time"
      Height          =   255
      Left            =   9000
      TabIndex        =   13
      Top             =   840
      Width           =   1815
   End
   Begin VB.OptionButton optByIx 
      Caption         =   "By Investigation"
      Height          =   255
      Left            =   9000
      TabIndex        =   12
      Top             =   480
      Width           =   1695
   End
   Begin VB.OptionButton optByPatient 
      Caption         =   "By Patient"
      Height          =   255
      Left            =   9000
      TabIndex        =   11
      Top             =   120
      Value           =   -1  'True
      Width           =   1695
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   11520
      TabIndex        =   8
      Top             =   6960
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1440
      TabIndex        =   5
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   104988675
      CurrentDate     =   39812
   End
   Begin VB.CheckBox chkToHadnover 
      Caption         =   "Handed-over to patient"
      Height          =   255
      Left            =   6000
      TabIndex        =   4
      Top             =   840
      Value           =   1  'Checked
      Width           =   3015
   End
   Begin VB.CheckBox chkPrinted 
      Caption         =   "Printouts taken"
      Height          =   255
      Left            =   6000
      TabIndex        =   3
      Top             =   480
      Width           =   1815
   End
   Begin VB.CheckBox chkToPrint 
      Caption         =   "To Take Printouts"
      Height          =   255
      Left            =   6000
      TabIndex        =   2
      Top             =   120
      Width           =   1815
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   1440
      TabIndex        =   6
      Top             =   600
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   104988675
      CurrentDate     =   39812
   End
   Begin btButtonEx.ButtonEx ButtonEx1 
      Height          =   495
      Left            =   120
      TabIndex        =   9
      Top             =   6960
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print List"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnIx 
      Height          =   495
      Left            =   1440
      TabIndex        =   10
      Top             =   6960
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Report"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   5655
      Left            =   120
      TabIndex        =   14
      Top             =   1200
      Width           =   12495
      _ExtentX        =   22040
      _ExtentY        =   9975
      _Version        =   393216
      Tabs            =   6
      TabsPerRow      =   6
      TabHeight       =   520
      TabCaption(0)   =   "Patient"
      TabPicture(0)   =   "frmTraceIx.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label4"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label5"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "cmbPatient"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "txtPatientID"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).ControlCount=   4
      TabCaption(1)   =   "Referring Doctor"
      TabPicture(1)   =   "frmTraceIx.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "txtRDocID"
      Tab(1).Control(1)=   "cmbRDoc"
      Tab(1).Control(2)=   "Label7"
      Tab(1).Control(3)=   "Label6"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Referring Institution"
      TabPicture(2)   =   "frmTraceIx.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "txtRInsID"
      Tab(2).Control(1)=   "cmbRIns"
      Tab(2).Control(2)=   "Label9"
      Tab(2).Control(3)=   "Label8"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Investigation"
      TabPicture(3)   =   "frmTraceIx.frx":0054
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "txtIxID"
      Tab(3).Control(1)=   "cmbIx"
      Tab(3).Control(2)=   "Label11"
      Tab(3).Control(3)=   "Label10"
      Tab(3).ControlCount=   4
      TabCaption(4)   =   "Department"
      TabPicture(4)   =   "frmTraceIx.frx":0070
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Label3"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).Control(1)=   "cmbDept"
      Tab(4).Control(1).Enabled=   0   'False
      Tab(4).ControlCount=   2
      TabCaption(5)   =   "All"
      TabPicture(5)   =   "frmTraceIx.frx":008C
      Tab(5).ControlEnabled=   0   'False
      Tab(5).ControlCount=   0
      Begin VB.TextBox txtIxID 
         Height          =   375
         Left            =   -66720
         TabIndex        =   32
         Top             =   480
         Width           =   1935
      End
      Begin VB.TextBox txtRInsID 
         Height          =   375
         Left            =   -66720
         TabIndex        =   28
         Top             =   480
         Width           =   1935
      End
      Begin VB.TextBox txtRDocID 
         Height          =   375
         Left            =   -66720
         TabIndex        =   24
         Top             =   480
         Width           =   1935
      End
      Begin VB.TextBox txtPatientID 
         Height          =   375
         Left            =   8280
         TabIndex        =   20
         Top             =   480
         Width           =   1935
      End
      Begin MSDataListLib.DataCombo cmbDept 
         Height          =   360
         Left            =   -72960
         TabIndex        =   15
         Top             =   480
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbPatient 
         Height          =   360
         Left            =   2040
         TabIndex        =   17
         Top             =   480
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbRDoc 
         Height          =   360
         Left            =   -72960
         TabIndex        =   21
         Top             =   480
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbRIns 
         Height          =   360
         Left            =   -72960
         TabIndex        =   25
         Top             =   480
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbIx 
         Height          =   360
         Left            =   -72960
         TabIndex        =   29
         Top             =   480
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin VB.Label Label11 
         Caption         =   "ID"
         Height          =   375
         Left            =   -67680
         TabIndex        =   31
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Investigation"
         Height          =   255
         Left            =   -74760
         TabIndex        =   30
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label9 
         Caption         =   "ID"
         Height          =   375
         Left            =   -67680
         TabIndex        =   27
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label8 
         Caption         =   "Referring Institution"
         Height          =   255
         Left            =   -74760
         TabIndex        =   26
         Top             =   480
         Width           =   2295
      End
      Begin VB.Label Label7 
         Caption         =   "ID"
         Height          =   375
         Left            =   -67680
         TabIndex        =   23
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label6 
         Caption         =   "Referring Doctor"
         Height          =   255
         Left            =   -74760
         TabIndex        =   22
         Top             =   480
         Width           =   1815
      End
      Begin VB.Label Label5 
         Caption         =   "ID"
         Height          =   375
         Left            =   7320
         TabIndex        =   19
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Patient"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Department :"
         Height          =   255
         Left            =   -74760
         TabIndex        =   16
         Top             =   480
         Width           =   1215
      End
   End
   Begin VB.Label Label2 
      Caption         =   "To:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "From :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmTraceIx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsIxList As New ADODB.Recordset
    Dim rsPatient As New ADODB.Recordset
    Dim i As Integer
    Public temReportID As Long
    Public temPatientIxID As Long
    
Private Sub Process()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub FillCombos()
    Dim Dept As New clsFillCombos
    Dept.FillAnyCombo cmbDept, "Dept", True
    Dim Doc As New clsFillCombos
    Doc.FillAnyCombo cmbRDoc, "Doctor", True
    Dim Ins As New clsFillCombos
    Ins.FillAnyCombo cmbRIns, "Institution", True
    Dim Ix As New clsFillCombos
    Ix.FillAnyCombo cmbIx, "Ix", True
    With rsPatient
        If .State = 1 Then .Close
        temSql = "Select * from tblPatient where Deleted = false order by Name"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With cmbPatient
        Set .RowSource = rsPatient
        .ListField = "Name"
        .BoundColumn = "PatientID"
    End With
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnIx_Click()
'    Dim temRow As Integer
'    Dim temPatientIxID As Long
'    Dim temPatientID As Long
'    Dim temIxID As Long
'    Dim temDoctorID As Long
'    Dim temInstitutionID As Long
'
'    Unload frmReport
'
'    With GridIx
'        If .Rows <= 1 Then Exit Sub
'        If .Row < 1 Then Exit Sub
'        temRow = .Row
'        If IsNumeric(.TextMatrix(temRow, 6)) = False Then Exit Sub
'        temPatientIxID = Val(.TextMatrix(temRow, 6))
'    End With
'    With rsIxList
'        If .State = 1 Then .Close
'        temSql = "Select * from tblPatientIx where PatientIxID = " & temPatientIxID
'        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
'        If .RecordCount > 0 Then
'            temPatientID = !PatientID
'            temIxID = !IxID
'            If IsNull(!DoctorID) = False Then
'                temDoctorID = !DoctorID
'            Else
'                temDoctorID = 0
'            End If
'            If IsNull(!InstitutionID) = False Then
'                temInstitutionID = !InstitutionID
'            Else
'                temInstitutionID = 0
'            End If
'        Else
'            MsgBox "Error"
'            Exit Sub
'        End If
'
'        If .State = 1 Then .Close
'        temSql = "Select * from tblPatientIx where PatientIxID = " & temPatientIxID
'        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
'        If .RecordCount = 1 Then
'            temReportID = !PatientIxID
'            .Close
'        ElseIf .RecordCount = 0 Then
'            .AddNew
'            !PatientID = temPatientID
'            !IxID = temIxID
'            !DoctorID = temDoctorID
'            !InstitutionID = temInstitutionID
'            !Date = Date
'            !Time = Time
'            !PatientIxID = temPatientIxID
'            .Update
'            temReportID = !ReportID
'        Else
'            MsgBox "Error"
'            Exit Sub
'        End If
'    End With
'    frmReport.Show
    Dim temRow As Integer
    Dim temPatientID As Long
    Dim temIxID As Long
    Dim temDoctorID As Long
    Dim temInstitutionID As Long
    
    Unload frmReportT
    
    With GridIx
        If .Rows <= 1 Then Exit Sub
        If .Row < 1 Then Exit Sub
        temRow = .Row
        If IsNumeric(.TextMatrix(temRow, 6)) = False Then Exit Sub
        temPatientIxID = Val(.TextMatrix(temRow, 6))
    End With
    With rsIxList
        If .State = 1 Then .Close
        temSql = "Select * from tblPatientIx where PatientIxID = " & temPatientIxID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            temPatientID = !PatientID
            temIxID = !IxID
            If IsNull(!DoctorID) = False Then
                temDoctorID = !DoctorID
            Else
                temDoctorID = 0
            End If
            If IsNull(!InstitutionID) = False Then
                temInstitutionID = !InstitutionID
            Else
                temInstitutionID = 0
            End If
        Else
            MsgBox "Error"
            Exit Sub
        End If
    End With
    frmReportT.Show
    frmReportT.WindowState = 2

End Sub

Private Sub chkPrinted_Click()
    Call Process
End Sub

Private Sub chkToHadnover_Click()
    Call Process
End Sub

Private Sub chkToPrint_Click()
    Call Process
End Sub

Private Sub cmbDept_Change()
    Call Process
End Sub

Private Sub cmbDept_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbDept.Text = Empty
    End If
End Sub

Private Sub cmbIx_Change()
    Call Process
End Sub

Private Sub cmbPatient_Change()
    Call Process
End Sub

Private Sub cmbRDoc_Change()
    Call Process
End Sub

Private Sub cmbRIns_Change()
    Call Process
End Sub

Private Sub dtpFrom_Change()
    Call Process
End Sub

Private Sub dtpTo_Change()
    Call Process
End Sub

Private Sub Form_Load()
    Call FillCombos
    Call FormatGrid
    dtpFrom.Value = DateSerial(Year(Date), 1, 1)
    dtpTo.Value = Date
    Call FillGrid
End Sub


Private Sub FormatGrid()
    With GridIx
        .Clear
        .Rows = 1
        .Cols = 8
        For i = 0 To .Cols - 1
            Select Case i
                Case 0:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 600
                    .Text = "No."
                Case 1:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 3000
                    .Text = "Patient"
                Case 2:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 3000
                    .Text = "Investigation"
                Case 3:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .Text = "Billing"
                Case 4:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .Text = "Printed"
                Case 5:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .Text = "Issued"
                Case 6:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 0
                    .Text = "ID"
                Case Else:
                    .ColWidth(i) = 0
            End Select
        Next
    End With
End Sub

Private Sub FillGrid()
    With rsIxList
        If .State = 1 Then .Close
        temSql = "SELECT tblPatient.Name, tblIx.Ix, tblPatientIx.* "
        temSql = temSql & "FROM tblPatient RIGHT JOIN (tblPatientIx LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID) ON tblPatient.PatientID = tblPatientIx.PatientID "
        temSql = temSql & "WHERE (((tblPatientIx.Deleted)=False) AND ( ((tblPatientIx.Billed)=False) "
        If chkPrinted.Value = 1 Then
            temSql = temSql & " OR ((tblPatientIx.Printed)=True) "
        End If
        If chkToHadnover.Value = 1 Then
            temSql = temSql & " OR ((tblPatientIx.Issued)=True) "
        End If
        If chkToPrint.Value = 1 Then
            temSql = temSql & " OR ((tblPatientIx.Printed)=False) "
        End If
        temSql = temSql & " ) "
        temSql = temSql & " AND ((tblPatientIx.BilledDate) Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "#) "
        
        Select Case SSTab1.Tab
            Case 0:
                    temSql = temSql & " AND ((tblPatientIx.PatientID)=" & Val(cmbPatient.BoundText) & "))"
            Case 1:
                    temSql = temSql & " AND ((tblPatientIx.DoctorID)=" & Val(cmbRDoc.BoundText) & "))"
            Case 2:
                    temSql = temSql & " AND ((tblPatientIx.InstitutionID)=" & Val(cmbRIns.BoundText) & "))"
            Case 3:
                    temSql = temSql & " AND ((tblIx.IxID)=" & Val(cmbIx.BoundText) & "))"
            Case 4:
                    temSql = temSql & " AND ((tblIx.DeptID)=" & Val(cmbDept.BoundText) & "))"
            Case 5:
                    temSql = temSql & ")"
        End Select
        
        If optByPatient.Value = True Then
            temSql = temSql & "ORDER BY tblPatient.Name"
        ElseIf optByIx.Value = True Then
            temSql = temSql & "ORDER BY tblIx.Ix"
        ElseIf optByID.Value = True Then
            temSql = temSql & "ORDER BY tblPatientIx.PatientIxID"
        End If
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            .MoveLast
            .MoveFirst
            GridIx.Rows = .RecordCount + 1
            For i = 1 To .RecordCount
                GridIx.TextMatrix(i, 0) = i
                GridIx.TextMatrix(i, 1) = Format(!Name, "")
                GridIx.TextMatrix(i, 2) = Format(!Ix, "")
                GridIx.TextMatrix(i, 3) = Format(!BilledDate, "dd MMM yyyy") & " - " & Format(!BilledTime, "HH : MM AMPM")
                GridIx.TextMatrix(i, 4) = Format(!PrintedDate, "dd MMM yyyy") & " - " & Format(!PrintedTime, "HH : MM AMPM")
                GridIx.TextMatrix(i, 5) = Format(!IssuedDate, "dd MMM yyyy") & " - " & Format(!IssuedTime, "HH : MM AMPM")
                GridIx.TextMatrix(i, 6) = !PatientIxID
                .MoveNext
            Next
        End If
        .Close
        
    End With
End Sub

Private Sub optByID_Click()
    Call Process

End Sub

Private Sub optByIx_Click()
    Call Process

End Sub

Private Sub optByPatient_Click()
    Call Process

End Sub

Private Sub SSTab1_Click(PreviousTab As Integer)
    Call Process
End Sub


Private Sub txtPatientID_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        cmbPatient.BoundText = Val(txtPatientID.Text)
    ElseIf KeyCode = vbKeyEscape Then
        txtPatientID.Text = Empty
    End If
End Sub

Private Sub txtIxID_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        cmbIx.BoundText = Val(txtIxID.Text)
    ElseIf KeyCode = vbKeyEscape Then
        txtIxID.Text = Empty
    End If
End Sub

Private Sub txtRDocID_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        cmbRDoc.BoundText = Val(txtRDocID.Text)
    ElseIf KeyCode = vbKeyEscape Then
        txtRDocID.Text = Empty
    End If
End Sub

Private Sub txtRInsID_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        cmbRIns.BoundText = Val(txtRInsID.Text)
    ElseIf KeyCode = vbKeyEscape Then
        txtRInsID.Text = Empty
    End If
End Sub
