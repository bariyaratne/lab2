VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Begin VB.MDIForm MDIMain 
   BackColor       =   &H8000000C&
   Caption         =   "Lakmedipro eLab"
   ClientHeight    =   9405
   ClientLeft      =   225
   ClientTop       =   855
   ClientWidth     =   14295
   Icon            =   "MDIMain.frx":0000
   LinkTopic       =   "MDIForm1"
   Picture         =   "MDIMain.frx":1242
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin ComctlLib.Toolbar Toolbar1 
      Align           =   3  'Align Left
      Height          =   9405
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1260
      _ExtentX        =   2223
      _ExtentY        =   16589
      ButtonWidth     =   1984
      ButtonHeight    =   2037
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   327682
      BeginProperty Buttons {0713E452-850A-101B-AFC0-4210102A8DA7} 
         NumButtons      =   7
         BeginProperty Button1 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Request new investigation"
            Object.Tag             =   ""
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "List of investigations"
            Object.Tag             =   ""
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Search"
            Object.Tag             =   ""
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Issue Reports"
            Object.Tag             =   ""
            ImageIndex      =   4
         EndProperty
         BeginProperty Button5 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Shift-end summery"
            Object.Tag             =   ""
            ImageIndex      =   5
         EndProperty
         BeginProperty Button6 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Day-end Summery"
            Object.Tag             =   ""
            ImageIndex      =   6
         EndProperty
         BeginProperty Button7 {0713F354-850A-101B-AFC0-4210102A8DA7} 
            Key             =   ""
            Object.ToolTipText     =   "Exit"
            Object.Tag             =   ""
            ImageIndex      =   7
         EndProperty
      EndProperty
   End
   Begin ComctlLib.ImageList ImageList1 
      Left            =   4800
      Top             =   3840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   68
      ImageHeight     =   71
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   7
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIMain.frx":35BB0
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIMain.frx":39496
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIMain.frx":3CD7C
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIMain.frx":4072E
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIMain.frx":44014
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIMain.frx":48126
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "MDIMain.frx":4BEC0
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuBackUp 
         Caption         =   "Back Up"
      End
      Begin VB.Menu mnuRestore 
         Caption         =   "Restore"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
      Begin VB.Menu mnuFileB 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   "&Edit"
      Begin VB.Menu mnuEditInvestigations 
         Caption         =   "Investigation"
         Begin VB.Menu mnuCategory 
            Caption         =   "Category"
         End
         Begin VB.Menu mnuEditTube 
            Caption         =   "Tube"
         End
         Begin VB.Menu mnuEditIx 
            Caption         =   "Investigations"
            Shortcut        =   {F8}
         End
         Begin VB.Menu mnuInvestigationFormat 
            Caption         =   "Investigation Format"
            Shortcut        =   {F9}
         End
         Begin VB.Menu mnuIxValueCatogeries 
            Caption         =   "Value Categories"
         End
         Begin VB.Menu mnuIxValues 
            Caption         =   "Values"
         End
         Begin VB.Menu mnuIxItemValues 
            Caption         =   "Assign Values to Investigations"
         End
         Begin VB.Menu mnuEditCalc 
            Caption         =   "Desing Calculation Fields"
         End
      End
      Begin VB.Menu mnuStaff 
         Caption         =   "Staff"
         Begin VB.Menu mnuStaffDetails 
            Caption         =   "Staff Details"
         End
         Begin VB.Menu mnuAuthority 
            Caption         =   "Positions"
         End
         Begin VB.Menu mnuEditStaffAuthorities 
            Caption         =   "Authorities"
         End
      End
      Begin VB.Menu mnuLabolatory 
         Caption         =   "Labolatory"
         Begin VB.Menu mnuDetails 
            Caption         =   "Details"
         End
         Begin VB.Menu mnuEditCompany 
            Caption         =   "Company"
         End
         Begin VB.Menu mnuEditRoom 
            Caption         =   "Room"
         End
         Begin VB.Menu mnuDepartment 
            Caption         =   "Departments"
         End
      End
      Begin VB.Menu mnuFinance 
         Caption         =   "Finance"
         Begin VB.Menu mnuExpence 
            Caption         =   "Expences"
         End
         Begin VB.Menu mnuIncome 
            Caption         =   "Income"
         End
         Begin VB.Menu mnuMaterials 
            Caption         =   "Materials"
         End
         Begin VB.Menu mnuUnits 
            Caption         =   "Units"
         End
      End
      Begin VB.Menu mnuReferrals 
         Caption         =   "Referrals"
         Begin VB.Menu mnuReferringDoctors 
            Caption         =   "Doctors"
         End
         Begin VB.Menu mnuInstitutions 
            Caption         =   "Institutions"
         End
      End
      Begin VB.Menu mnuReportFormat 
         Caption         =   "Report Format"
      End
      Begin VB.Menu mnuEditMyDetails 
         Caption         =   "My Details"
      End
      Begin VB.Menu mnuEditB 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuPatients 
      Caption         =   "&Reports"
      Begin VB.Menu mnuReportRequests 
         Caption         =   "Requests"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuLists 
         Caption         =   "Lists"
         Shortcut        =   {F3}
      End
      Begin VB.Menu mnuReportsSearch 
         Caption         =   "Search"
         Shortcut        =   {F4}
      End
      Begin VB.Menu mnuIssue 
         Caption         =   "Issue"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuRequestsCancell 
         Caption         =   "Cancell"
      End
      Begin VB.Menu mnuTraceIx 
         Caption         =   "Trace Investigation"
      End
      Begin VB.Menu mnuReportsB 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuBackOffice 
      Caption         =   "&Back Office"
      Begin VB.Menu mnuBackOfficeDayEndSummery 
         Caption         =   "Day End Summery"
         Shortcut        =   {F6}
      End
      Begin VB.Menu mnuBackOfficeShiftEndSummery 
         Caption         =   "Shift End Summery"
         Shortcut        =   {F7}
      End
      Begin VB.Menu mnuBackOfficeDoctorSummery 
         Caption         =   "Doctor Summery"
      End
      Begin VB.Menu mnuBackOfficeStaffSummery 
         Caption         =   "Staff Summery"
      End
      Begin VB.Menu mnuBackOfficeInvestigationsReports 
         Caption         =   "Investigations Report"
      End
      Begin VB.Menu mnuBackOfficeInvestigationSummery 
         Caption         =   "Investigation Summery"
      End
      Begin VB.Menu mnuBackOfficeDailySummery 
         Caption         =   "Daily Summery"
      End
      Begin VB.Menu mnuBackOfficeB 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuPreferences 
      Caption         =   "&Preferences"
      Begin VB.Menu mnuInitialPreferences 
         Caption         =   "Database"
      End
      Begin VB.Menu mnuInstitution 
         Caption         =   "Institution"
      End
      Begin VB.Menu mnuPrintingPreferances 
         Caption         =   "Printing Preferances"
      End
      Begin VB.Menu mnuPreferanceB 
         Caption         =   "-"
      End
   End
   Begin VB.Menu mnuWindow 
      Caption         =   "Window"
      WindowList      =   -1  'True
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About"
      End
   End
End
Attribute VB_Name = "MDIMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub frmInstitution_Click()
    frmInstitution.Show
    frmInstitution.ZOrder 0
End Sub

Private Sub MDIForm_Load()
    Call VisibleControls(Me)
End Sub


Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim i As Integer
    i = MsgBox("Are you sure you want to exit?", vbYesNo, "Exit?")
    If i = vbNo Then Cancel = True
End Sub

Private Sub mnuAuthority_Click()
    frmPosition.Show
    frmPosition.ZOrder 0
End Sub

Private Sub mnuBackOfficeCashInvestigationsReports_Click()
    frmCashInvestigationsReport.Show
    frmCashInvestigationsReport.ZOrder 0
End Sub

Private Sub mnuBackOfficeCashInvestigationSummery_Click()
    frmCashInvestigationSummery.Show
    frmCashInvestigationSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeCreditInvestigationsReport_Click()
    frmCreditInvestigationsReport.Show
    frmCreditInvestigationsReport.ZOrder 0
End Sub

Private Sub mnuBackOfficeCreditInvestigationSummery_Click()
    frmCreditInvestigationSummery.Show
    frmCreditInvestigationSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeDailySummery_Click()
    frmDailySummery.Show
    frmDailySummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeDayEndSummery_Click()
    frmDayEndSummery.Show
    frmDayEndSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeDoctorSummery_Click()
    frmDoctorSummery.Show
    frmDoctorSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeInvestigationsReports_Click()
    frmInvestigationsReport.Show
    frmInvestigationsReport.ZOrder 0
End Sub

Private Sub mnuBackOfficeInvestigationSummery_Click()
    frmInvestigationSummery.Show
    frmInvestigationSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeShiftEndSummery_Click()
    frmShiftEndSummery.Show
    frmShiftEndSummery.ZOrder 0
End Sub

Private Sub mnuBackOfficeStaffSummery_Click()
    frmStaffIxLists.Show
    frmStaffIxLists.ZOrder 0
End Sub

Private Sub mnuBackOfficeWardInvestigationsReport_Click()
    frmWardInvestigationsReport.Show
    frmWardInvestigationsReport.ZOrder 0
End Sub

Private Sub mnuBackOfficeWardInvestigationSummery_Click()
    frmWardInvestigationSummery.Show
    frmWardInvestigationSummery.ZOrder 0
End Sub

Private Sub mnuBackUp_Click()
    frmBackUp.Show
    frmBackUp.ZOrder 0
End Sub

Private Sub mnuCategory_Click()
    frmCategory.Show
    frmCategory.ZOrder 0
End Sub

Private Sub mnuDepartment_Click()
    frmDept.Show
    frmDept.ZOrder 0
End Sub

Private Sub mnuDetails_Click()
    frmInstitution.Show
    frmInstitution.ZOrder 0
End Sub

Private Sub mnuEditCalc_Click()
    frmCalc.Show
    frmCalc.ZOrder 0
End Sub

Private Sub mnuEditCompany_Click()
    frmCompany.Show
    frmCompany.ZOrder 0
End Sub

Private Sub mnuEditIx_Click()
    frmIx.Show
    frmIx.ZOrder 0
End Sub

Private Sub mnuEditIxDetails_Click()
    frmIxDetails.Show
    frmIxDetails.ZOrder 0
End Sub

Private Sub mnuEditMyDetails_Click()
    frmMyDetails.Show
    frmMyDetails.ZOrder 0
End Sub

Private Sub mnuEditRoom_Click()
    frmRoom.Show
    frmRoom.ZOrder 0
End Sub

Private Sub mnuEditStaffAuthorities_Click()
    frmAuthorityPrevilagesMenuVisible.Show
    frmAuthorityPrevilagesMenuVisible.ZOrder 0
End Sub

Private Sub mnuEditTube_Click()
    frmTube.Show
    frmTube.ZOrder 0
End Sub

Private Sub mnuExit_Click()
    Unload Me
End Sub

Private Sub mnuExpence_Click()
    frmExpence.Show
    frmExpence.ZOrder 0
End Sub

Private Sub mnuFDayEndSummery_Click()
        frmDayEndSummery.Show
        frmDayEndSummery.ZOrder 0
End Sub

Private Sub mnuFShiftEndSummery_Click()
    frmFShiftEndSummery.Show
    frmFShiftEndSummery.ZOrder 0
End Sub

Private Sub mnuHelp_Click()
'    frmTem.Show
    
End Sub

Private Sub mnuHelpAbout_Click()
'    frmTem.Show
'    frmTem.ZOrder 0
End Sub

Private Sub mnuIncome_Click()
    frmIncome.Show
    frmIncome.ZOrder 0
End Sub

Private Sub mnuInitialPreferences_Click()
    frmSelectDatabase.Show
    frmSelectDatabase.ZOrder 0
End Sub

Private Sub mnuInstitution_Click()
    frmInstitution.Show
    frmInstitution.ZOrder 0
End Sub

Private Sub mnuInstitutions_Click()
    frmInstitutions.Show
    frmInstitutions.ZOrder 0
End Sub

Private Sub mnuInvestigationFormat_Click()
    frmIxFormat.Show
    frmIxFormat.ZOrder 0
End Sub

Private Sub mnuIssue_Click()
    frmIssue.Show
    frmIssue.ZOrder 0
    frmIssue.Top = 0
    frmIssue.Left = 0
End Sub

Private Sub mnuIxItemValues_Click()
    frmIxItemValues.Show
    frmIxItemValues.ZOrder 0
End Sub

Private Sub mnuIxValueCatogeries_Click()
    frmValueCategory.Show
    frmValueCategory.ZOrder 0
End Sub

Private Sub mnuIxValues_Click()
    frmIxValues.Show
    frmIxValues.ZOrder 0
End Sub

Private Sub mnuLists_Click()
    frmIxLists.Show
    frmIxLists.ZOrder 0
    frmIxLists.Top = 0
    frmIxLists.Left = 0
End Sub

Private Sub mnuMaterials_Click()
    frmMaterials.Show
    frmMaterials.ZOrder 0
End Sub

Private Sub mnuPatient_Click()
    frmPatient.Show
    frmPatient.ZOrder 0
End Sub

Private Sub mnuMaterialUsage_Click()
    frmMaterialUsage.Show
    frmMaterialUsage.ZOrder 0
End Sub

Private Sub mnuPatientsAddEditPatients_Click()
    frmPatient.Show
    frmPatient.ZOrder 0
End Sub

Private Sub mnuPatientsInvestigations_Click()
    frmPatientIx.Show
    frmPatientIx.ZOrder 0
End Sub

Private Sub mnuPrintingPreferances_Click()
    frmPrintingPreferances.Show
    frmPrintingPreferances.ZOrder 0
End Sub

Private Sub mnuProgrammePreferances_Click()
    frmSelectDatabase.Show
    frmSelectDatabase.ZOrder 0
End Sub

Private Sub mnuReferringDoctors_Click()
    frmDoctor.Show
    frmDoctor.ZOrder 0
End Sub

Private Sub mnuReportFormat_Click()
    frmReportFormat.Show
    frmReportFormat.ZOrder 0

'    frmReportFormatNew.Show
'    frmReportFormatNew.ZOrder 0
End Sub

Private Sub mnuReportRequests_Click()
    frmPatientIxNew.Show
    frmPatientIxNew.ZOrder 0
End Sub

Private Sub mnuReportsSearch_Click()
    frmIxSearch.Show
    frmIxSearch.ZOrder 0
End Sub

Private Sub mnuRequestsCancell_Click()
    frmCancellBillSearch.Show
    frmCancellBillSearch.ZOrder 0
End Sub

Private Sub mnuRestore_Click()
    frmRestore.Show
    frmRestore.ZOrder 0
End Sub

Private Sub mnuStaffDetails_Click()
    frmStaffDetails.Show
    frmStaffDetails.ZOrder 0
End Sub

Private Sub mnuTraceIx_Click()
    frmTraceIx.Show
    frmTraceIx.ZOrder 0
End Sub

Private Sub mnuUnits_Click()
    frmUnits.Show
    frmUnits.ZOrder 0
End Sub


Private Sub Toolbar1_ButtonClick(ByVal Button As ComctlLib.Button)
    Select Case Button.Index
        Case 1: If mnuReportRequests.Enabled = True And mnuReportRequests.Visible = True Then mnuReportRequests_Click
        Case 2: If mnuLists.Enabled = True And mnuLists.Visible = True Then mnuLists_Click
        Case 3: If mnuReportsSearch.Enabled = True And mnuReportsSearch.Visible = True Then mnuReportsSearch_Click
        Case 4: If mnuIssue.Enabled = True And mnuIssue.Visible = True Then mnuIssue_Click
        Case 5: If mnuBackOfficeShiftEndSummery.Enabled = True And mnuBackOfficeShiftEndSummery.Visible = True Then mnuBackOfficeShiftEndSummery_Click
        Case 6: If mnuBackOfficeDayEndSummery.Enabled = True And mnuBackOfficeDayEndSummery.Visible = True Then mnuBackOfficeDayEndSummery_Click
        Case 7: Unload Me
    End Select
End Sub
