VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmStaffDetails 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Staff Details"
   ClientHeight    =   5280
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10335
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5280
   ScaleWidth      =   10335
   Begin VB.Frame fra2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   4080
      TabIndex        =   14
      Top             =   120
      Width           =   6135
      Begin MSDataListLib.DataCombo dtcPosition 
         Height          =   360
         Left            =   1440
         TabIndex        =   7
         Top             =   3120
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin VB.TextBox txtName 
         Height          =   375
         Left            =   1440
         TabIndex        =   1
         Top             =   240
         Width           =   4575
      End
      Begin VB.TextBox txtAddress 
         Height          =   375
         Left            =   1440
         TabIndex        =   2
         Top             =   720
         Width           =   4575
      End
      Begin VB.TextBox txtPhone 
         Height          =   375
         Left            =   1440
         TabIndex        =   3
         Top             =   1200
         Width           =   4575
      End
      Begin VB.TextBox txtMobile 
         Height          =   375
         Left            =   1440
         TabIndex        =   4
         Top             =   1680
         Width           =   4575
      End
      Begin VB.TextBox txtUserName 
         Height          =   375
         Left            =   1440
         TabIndex        =   5
         Top             =   2160
         Width           =   4575
      End
      Begin VB.TextBox txtPassword 
         Height          =   375
         IMEMode         =   3  'DISABLE
         Left            =   1440
         PasswordChar    =   "*"
         TabIndex        =   6
         Top             =   2640
         Width           =   4575
      End
      Begin btButtonEx.ButtonEx bttnSave 
         Height          =   375
         Left            =   2400
         TabIndex        =   8
         Top             =   3840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnChange 
         Height          =   375
         Left            =   2400
         TabIndex        =   15
         Top             =   3840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   375
         Left            =   4080
         TabIndex        =   9
         Top             =   3840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "Position"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   3120
         Width           =   1455
      End
      Begin VB.Label lblName 
         Caption         =   "Name"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblAddress 
         Caption         =   "Address"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lblPhone 
         Caption         =   "Phone"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label lblMobile 
         Caption         =   "Mobile"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1800
         Width           =   1455
      End
      Begin VB.Label lblUserName 
         Caption         =   "User Name"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   2280
         Width           =   1455
      End
      Begin VB.Label lblPassword 
         Caption         =   "Password"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   2760
         Width           =   1455
      End
   End
   Begin VB.Frame fra1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3615
      Begin btButtonEx.ButtonEx bttnEdit 
         Height          =   375
         Left            =   1320
         TabIndex        =   13
         Top             =   3840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Edit"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnAdd 
         Height          =   375
         Left            =   120
         TabIndex        =   12
         Top             =   3840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo dtcStaffDetails 
         Height          =   3300
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   5821
         _Version        =   393216
         Style           =   1
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnDelete 
         Height          =   375
         Left            =   2520
         TabIndex        =   23
         Top             =   3840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Delete"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   9240
      TabIndex        =   10
      Top             =   4800
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmStaffDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsStaffDetails As New ADODB.Recordset
    Dim rsViewStaffDetails As New ADODB.Recordset
    Dim rsComments As New ADODB.Recordset
    Dim rsViewPosition As New ADODB.Recordset
    
    Dim temSql As String
    
Private Sub AfterAdd()
    
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    dtcStaffDetails.Enabled = False
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = True
    bttnChange.Enabled = False
    bttnCancel.Enabled = True
    txtName.Enabled = True
    txtAddress.Enabled = True
    txtPhone.Enabled = True
    txtMobile.Enabled = True
    txtUserName.Enabled = True
    txtPassword.Enabled = True
    dtcPosition.Enabled = True
    
    bttnSave.Visible = True
    bttnChange.Visible = False
    
End Sub
Private Sub AfterEdit()
    
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    dtcStaffDetails.Enabled = False
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = False
    bttnChange.Enabled = True
    bttnCancel.Enabled = True
    txtName.Enabled = True
    txtAddress.Enabled = True
    txtPhone.Enabled = True
    txtMobile.Enabled = True
    txtUserName.Enabled = True
    txtPassword.Enabled = True
    dtcPosition.Enabled = True
    
    bttnSave.Visible = False
    bttnChange.Visible = True
    
End Sub

Private Sub BeforeAddEdit()
    
    bttnAdd.Enabled = True
    bttnEdit.Enabled = False
    dtcStaffDetails.Enabled = True
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = False
    bttnChange.Enabled = False
    bttnCancel.Enabled = False
    txtName.Enabled = False
    txtAddress.Enabled = False
    txtPhone.Enabled = False
    txtMobile.Enabled = False
    txtUserName.Enabled = False
    txtPassword.Enabled = False
    dtcPosition.Enabled = False
    
    bttnSave.Visible = True
    bttnChange.Visible = True
    
    On Error Resume Next
    dtcStaffDetails.SetFocus
    
End Sub

Private Sub Clearvalues()
    txtName.Text = Empty
    txtAddress.Text = Empty
    txtPhone.Text = Empty
    txtMobile.Text = Empty
    txtUserName.Text = Empty
    txtPassword.Text = Empty
    dtcPosition.Text = Empty
End Sub

Private Sub bttnAdd_Click()
    If UserPositionID <> 1 Then
        MsgBox "Only the Administrator can Add"
        Exit Sub
    End If
    
    
    Clearvalues
    AfterAdd
    txtName.SetFocus
    txtName.Text = dtcStaffDetails.Text
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnCancel_Click()
    Call BeforeAddEdit
    Call Clearvalues
    dtcStaffDetails.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnChange_Click()
    If CanAdd = False Then Exit Sub
    Dim TemResponce As Integer
    With rsStaffDetails
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblStaff Where StaffID = " & Val(dtcStaffDetails.BoundText), cnnLab, adOpenStatic, adLockOptimistic
        !Name = txtName.Text
        If .RecordCount = 0 Then Exit Sub
        !Name = Trim(txtName.Text)
        !Address1 = txtAddress.Text
        !Telephone = txtPhone.Text
        !Mobile = txtMobile.Text
        !UserName = EncreptedWord(txtUserName.Text)
        !Password = EncreptedWord(txtPassword.Text)
        !PositionID = dtcPosition.BoundText
        .Update
        If .State = 1 Then .Close
        FillCOmbos
        BeforeAddEdit
        Clearvalues
        dtcStaffDetails.Text = Empty
        dtcStaffDetails.SetFocus
        Exit Sub
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        Clearvalues
        BeforeAddEdit
        dtcStaffDetails.Text = Empty
        dtcStaffDetails.SetFocus
        If .State = 1 Then .Close
    End With
End Sub
    

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnDelete_Click()
    If UserPositionID <> 1 Then
        MsgBox "Only the Administrator can delete"
        Exit Sub
    End If
    
    With rsStaffDetails
        If .State = 1 Then .Close
        temSql = "Delete * from tblStaff where StaffID = " & Val(dtcStaffDetails.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Close
    End With
    Call FillCOmbos
    dtcStaffDetails.Text = Empty
End Sub

Private Sub bttnEdit_Click()
    If UserPositionID <> 1 Then
        If UserID <> dtcStaffDetails.BoundText Then
            MsgBox "You can't edit others details"
            Exit Sub
        End If
    End If
    
    AfterEdit
    txtName.SetFocus

On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnSave_Click()
    If CanAdd = False Then Exit Sub
    Dim TemResponce As Integer
    With rsStaffDetails
        On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblStaff", cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Name = Trim(txtName.Text)
        !Address1 = txtAddress.Text
        !Telephone = txtPhone.Text
        !Mobile = txtMobile.Text
        !UserName = EncreptedWord(txtUserName.Text)
        !Password = EncreptedWord(txtPassword.Text)
        !PositionID = dtcPosition.BoundText
        .Update
        If .State = 1 Then .Close
        FillCOmbos
        BeforeAddEdit
        Clearvalues
        dtcStaffDetails.Text = Empty
        dtcStaffDetails.SetFocus
        Exit Sub
    
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        Clearvalues
        BeforeAddEdit
        dtcStaffDetails.Text = Empty
        dtcStaffDetails.SetFocus
        If .State = 1 Then .Close
    End With
End Sub

Private Function CanAdd() As Boolean
    CanAdd = False
    Dim tr As Integer
    If Trim(txtName.Text) = Empty Then
        tr = MsgBox("You have not entered the Name", vbCritical, "No Name")
        txtName.SetFocus
        Exit Function
    End If
    If IsNumeric(dtcPosition.BoundText) = False Then
        tr = MsgBox("You have not selected an Position", vbCritical, "Position")
        dtcPosition.SetFocus
        Exit Function
    End If
    If UserPositionID <> 1 And Val(dtcPosition.BoundText) = 1 Then
        tr = MsgBox("You have not allowed to set the Position as an administrator", vbCritical, "Not Authorised")
        dtcPosition.SetFocus
        Exit Function
    End If
    CanAdd = True
End Function

Private Sub dtcStaffDetails_Change()
    If IsNumeric(dtcStaffDetails.BoundText) = True Then
        bttnEdit.Enabled = True
        bttnAdd.Enabled = False
        bttnDelete.Enabled = True
        Clearvalues
        DisplaySelected
    Else
        bttnEdit.Enabled = False
        bttnAdd.Enabled = True
        bttnDelete.Enabled = False
        Clearvalues
    End If
End Sub


Private Sub Form_Load()
    If UserPositionID = 1 Or UserPositionID = 2 Then
    
    Else
        bttnSave.Enabled = False
        bttnChange.Enabled = False
    End If
    FillCOmbos
    BeforeAddEdit
    Clearvalues
End Sub
Private Sub FillCOmbos()
    With rsViewStaffDetails
        If .State = 1 Then .Close
        temSql = "Select * From tblStaff Order By Name"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcStaffDetails
        Set .RowSource = rsViewStaffDetails
        .ListField = "Name"
        .BoundColumn = "StaffID"
    End With
    With rsViewPosition
        If .State = 1 Then .Close
        temSql = "SELECT tblPosition.* FROM tblPosition"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcPosition
        Set .RowSource = rsViewPosition
        .ListField = "Position"
        .BoundColumn = "PositionID"
    End With
    
End Sub

Private Sub NoName()
    Dim TemResponce As Integer
    TemResponce = MsgBox("You have not entered a Name to save", vbCritical, "No Name")
    txtName.SetFocus
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If rsStaffDetails.State = 1 Then rsStaffDetails.Close: Set rsStaffDetails = Nothing
    If rsViewStaffDetails.State = 1 Then rsViewStaffDetails.Close: Set rsViewStaffDetails = Nothing
End Sub

Private Sub DisplaySelected()
    If Not IsNumeric(dtcStaffDetails.BoundText) Then Exit Sub
    With rsStaffDetails
        If .State = 1 Then .Close
        .Open "Select * From tblStaff Where (StaffID = " & dtcStaffDetails.BoundText & ")", cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Call Clearvalues
        If Not IsNull(!Name) Then txtName.Text = !Name
        If Not IsNull(!Address1) Then txtAddress.Text = !Address1
        If Not IsNull(!Telephone) Then txtPhone.Text = !Telephone
        If Not IsNull(!Mobile) Then txtMobile.Text = !Mobile
        If Not IsNull(!UserName) Then txtUserName.Text = DecreptedWord(!UserName)
        If Not IsNull(!Password) Then txtPassword.Text = DecreptedWord(!Password)
        If Not IsNull(!PositionID) Then dtcPosition.BoundText = !PositionID
        If .State = 1 Then .Close
    End With
End Sub
