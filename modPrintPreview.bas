Attribute VB_Name = "modPrintPreview"
Option Explicit
    Dim PrinterToPreviewPrinterRatio As Double
    

Public Sub PreviewReportText(ByVal SuppliedText As String, X1 As Long, X2 As Long, Y1 As Long, Y2 As Long, PrintFontName As String, PrintFontSize As Long, PrintFontBold As Boolean, PrintFontItalic As Boolean, PrintFontUnderline As Boolean, PrintFontStrikethrough As Boolean, PrintAlignment As Integer, ByRef MyForm As frmPrintPreview)
    Dim NewString As String
    Dim PrintString As String
    Dim COuntUp As Long
    Dim UpPosition As Long
    Dim UpSpacePosition As Long
    Dim NewStringLength As Long
    Dim PrintedString As String
    Dim MyX1 As Single
    
    MyX1 = X1
    
    
    PrinterToPreviewPrinterRatio = 0.5

    
    NewString = SuppliedText
    PrintString = SuppliedText
    
    MyForm.Font.Name = PrintFontName
    MyForm.Font.Size = PrintFontSize * PrinterToPreviewPrinterRatio
    MyForm.Font.Bold = PrintFontBold
    MyForm.Font.Underline = PrintFontUnderline
    MyForm.Font.Strikethrough = PrintFontStrikethrough
    
    
    While InStr(NewString, "^") > 0
        UpPosition = InStr(NewString, "^")
        NewStringLength = Len(NewString)
        UpSpacePosition = InStr(UpPosition, NewString, " ")
        If UpSpacePosition = 0 Then UpSpacePosition = NewStringLength
        COuntUp = COuntUp + 1
        PrintString = Left(NewString, UpSpacePosition)
        PrintedString = PrintedString + PrintString
        NewString = Right(NewString, NewStringLength - UpPosition - 1)
        
        MyX1 = MyX1 + MyForm.TextWidth(PrintedString)
        
        SuperScriptPreview PrintString, MyX1, X2, Y1, Y2, PrintFontName, PrintFontSize, PrintFontBold, PrintFontItalic, PrintFontUnderline, PrintFontStrikethrough, TextAlignment.LeftAlign, MyForm
        PrintString = ""
'        SuperScriptPrint SuppliedText, X1, X2, Y1, Y2, PrintFontName, PrintFontSize, PrintFontBold, PrintFontItalic, PrintFontUnderline, PrintFontStrikethrough, PrintAlignment
    Wend
    
    SuperScriptPreview PrintString, MyX1, X2, Y1, Y2, PrintFontName, PrintFontSize, PrintFontBold, PrintFontItalic, PrintFontUnderline, PrintFontStrikethrough, PrintAlignment, MyForm
    
End Sub
    
    
Private Sub SuperScriptPreview(ByVal SuppliedText As String, ByVal X1 As Long, ByVal X2 As Long, ByVal Y1 As Long, ByVal Y2 As Long, PrintFontName As String, PrintFontSize As Long, PrintFontBold As Boolean, PrintFontItalic As Boolean, PrintFontUnderline As Boolean, PrintFontStrikethrough As Boolean, PrintAlignment As Integer, ByRef MyForm As frmPrintPreview)
    Dim SuperscriptPresent As Boolean
    Dim SuperscriptLocation As Long
    Dim BeforeSuperscriptText As String
    Dim AfterSuperscriptText As String
    Dim SuperScriptText As String
    Dim SpaceLocation As Long
    Dim SuperscriptElevation As Double
    
    Dim TotalTextWidth As Long
    Dim TextWidth1 As Long
    Dim TextWidth2 As Long
    Dim TextWidth3 As Long
    
    SuperscriptLocation = InStr(1, SuppliedText, "^")
    SuperscriptElevation = 0.2
    
    If SuperscriptLocation = 0 Then
        MyForm.Font.Name = PrintFontName
        MyForm.Font.Size = PrintFontSize * PrinterToPreviewPrinterRatio
        MyForm.Font.Bold = PrintFontBold
        MyForm.Font.Underline = PrintFontUnderline
        MyForm.Font.Strikethrough = PrintFontStrikethrough
        MyForm.CurrentY = Y1
        If PrintAlignment = TextAlignment.LeftAlign Then
            MyForm.CurrentX = X1
        ElseIf PrintAlignment = TextAlignment.RightAlign Then
            MyForm.CurrentX = X2 - MyForm.TextWidth(SuppliedText)
        ElseIf PrintAlignment = TextAlignment.CenterAlign Then
            MyForm.CurrentX = ((X1 + X2) / 2) - (MyForm.TextWidth(SuppliedText) / 2)
        Else
            MyForm.CurrentX = ((X1 + X2) / 2) - (MyForm.TextWidth(SuppliedText) / 2)
        End If
        MyForm.Print SuppliedText
    Else
        If SuperscriptLocation > 0 Then BeforeSuperscriptText = Left(SuppliedText, SuperscriptLocation - 1)
        
        AfterSuperscriptText = Right(SuppliedText, (Len(SuppliedText) - SuperscriptLocation))
        
        If InStr(1, AfterSuperscriptText, " ") = 0 Then
            
            AfterSuperscriptText = Trim(AfterSuperscriptText)
            
            MyForm.Font.Name = PrintFontName
            MyForm.Font.Size = PrintFontSize * PrinterToPreviewPrinterRatio
            MyForm.Font.Bold = PrintFontBold
            MyForm.Font.Underline = PrintFontUnderline
            MyForm.Font.Strikethrough = PrintFontStrikethrough
            
            TextWidth1 = MyForm.TextWidth(BeforeSuperscriptText)
            
            MyForm.FontSize = PrintFontSize - 2
            
            TextWidth2 = MyForm.TextWidth(AfterSuperscriptText)
            
            TextWidth3 = 0
            
            TotalTextWidth = TextWidth1 + TextWidth2 + TextWidth3
            
            MyForm.Font.Name = PrintFontName
            MyForm.Font.Size = PrintFontSize * PrinterToPreviewPrinterRatio
            MyForm.Font.Bold = PrintFontBold
            MyForm.Font.Underline = PrintFontUnderline
            MyForm.Font.Strikethrough = PrintFontStrikethrough
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                MyForm.CurrentX = X1
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                MyForm.CurrentX = X2 - TotalTextWidth
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2)
            Else
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2)
            End If
            
            MyForm.CurrentY = Y1
            MyForm.Print BeforeSuperscriptText
            
            
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                MyForm.CurrentX = X1 + TextWidth1
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                MyForm.CurrentX = X2 - TotalTextWidth + TextWidth1
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1
            Else
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1
            End If
            MyForm.CurrentY = Y1 - (MyForm.TextHeight(BeforeSuperscriptText) * SuperscriptElevation)
            MyForm.FontSize = PrintFontSize - 2
            
            MyForm.Print AfterSuperscriptText
            
            MyForm.FontSize = PrintFontSize
            
        Else
            SpaceLocation = InStr(1, AfterSuperscriptText, " ")
            SuperScriptText = Left(AfterSuperscriptText, SpaceLocation)
            AfterSuperscriptText = Right(AfterSuperscriptText, (Len(AfterSuperscriptText) - SpaceLocation))
            MyForm.Font.Name = PrintFontName
            MyForm.Font.Size = PrintFontSize * PrinterToPreviewPrinterRatio
            MyForm.Font.Bold = PrintFontBold
            MyForm.Font.Underline = PrintFontUnderline
            MyForm.Font.Strikethrough = PrintFontStrikethrough
            
            TextWidth1 = MyForm.TextWidth(BeforeSuperscriptText)
            MyForm.FontSize = PrintFontSize - 2
            TextWidth2 = MyForm.TextWidth(SuperScriptText)
            MyForm.FontSize = PrintFontSize
            TextWidth3 = MyForm.TextWidth(AfterSuperscriptText)
            TotalTextWidth = TextWidth1 + TextWidth2 + TextWidth3
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                MyForm.CurrentX = X1
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                MyForm.CurrentX = X2 - TotalTextWidth
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2)
            Else
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2)
            End If
            
            MyForm.CurrentY = Y1
            MyForm.Print BeforeSuperscriptText
            
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                MyForm.CurrentX = X1 + TextWidth1
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                MyForm.CurrentX = X2 - TotalTextWidth + TextWidth1
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1
            Else
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1
            End If
            
            
            MyForm.CurrentY = Y1 - (MyForm.TextHeight(BeforeSuperscriptText) * SuperscriptElevation)
            MyForm.FontSize = PrintFontSize - 2
            MyForm.Print SuperScriptText
            
            
            
            MyForm.FontSize = PrintFontSize
            MyForm.CurrentY = Y1
            
            If PrintAlignment = TextAlignment.LeftAlign Then
                MyForm.CurrentX = X1 + TextWidth1 + TextWidth2
            ElseIf PrintAlignment = TextAlignment.RightAlign Then
                MyForm.CurrentX = X2 - TotalTextWidth + TextWidth1 + TextWidth2
            ElseIf PrintAlignment = TextAlignment.CenterAlign Then
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1 + TextWidth2
            Else
                MyForm.CurrentX = ((X1 + X2) / 2) - (TotalTextWidth / 2) + TextWidth1 + TextWidth2
            End If
            
            MyForm.Print AfterSuperscriptText
        End If
    End If
End Sub

