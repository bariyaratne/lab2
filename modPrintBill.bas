Attribute VB_Name = "modPrintBill"
Option Explicit
Dim cSetDfltPrinter As New cSetDfltPrinter



Public Sub printLabBillPos(BillID As Long, PrinterName As String, PaperName As String, formHdc)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyBill As New PatientIxBill
    Dim MyPatient As New Patient
    Dim myPtIx As New PatientIx
    Dim myBillItem As New PatientIxItem
    Dim MyIx As New Ix
    Dim myTitle As New Title
    Dim myStaff As New Staff
    Dim myLab As New LabDetails
    
    MyBill.PatientIxBillID = BillID
    MyPatient.PatientID = MyBill.PatientID
    myTitle.TitleID = MyPatient.TitleID
    myLab.LabID = 1
    myStaff.StaffID = MyBill.UserID
    
    csetPrinter.SetPrinterAsDefault PrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = PrinterName Then
            Set Printer = myP
        End If
    Next
    
    If SelectForm(PaperName, formHdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Tahoma"
    
    Printer.FontSize = 15
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Name)) / 2)
    Printer.Print DecreptedWord(myLab.Name)

    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.RegNo)) / 2)
    Printer.Print DecreptedWord(myLab.RegNo)

    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Address)) / 2)
    Printer.Print DecreptedWord(myLab.Address)
    
    Printer.FontSize = 12
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Telephone1)) / 2)
    Printer.Print DecreptedWord(myLab.Telephone1)
    
    Printer.FontSize = 11
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Email)) / 2)
    Printer.Print DecreptedWord(myLab.Email)
    
    
    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2)
    Printer.Print "Laboratory Receipt"

    Dim displacement As Integer
    
    displacement = 0
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 11
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & myTitle.Title & " " & MyPatient.Name
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.Time, "hh:mm AMPM")) + 740)
    Printer.Print Format(MyBill.Time, "hh:mm AMPM")
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.PatientIxBillID
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.billDate, "dd MMM yyyy")) + 740)
    Printer.Print Format(MyBill.billDate, "dd MMM yyyy")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 50 + displacement
    Tab4 = 36 + displacement
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSql = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.IxDisplay, 38); Tab(Tab3); Right((Space(10)) & Format(myPtIx.Value, "0.00"), 10)
            Printer.Print
            .MoveNext
        Wend
        .Close
    End With
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 40 + displacement
    Tab4 = 46 + displacement
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Value, "0.00")), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetValue)), 10)
        End If
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        .EndDoc
    End With
End Sub

Public Sub printCancellBillPos(BillID As Long, PrinterName As String, PaperName As String, formHdc)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyBill As New PatientIxBill
    Dim MyPatient As New Patient
    Dim myPtIx As New PatientIx
    Dim myBillItem As New PatientIxItem
    Dim MyIx As New Ix
    Dim myTitle As New Title
    Dim myStaff As New Staff
    Dim myLab As New LabDetails
    
    MyBill.PatientIxBillID = BillID
    MyPatient.PatientID = MyBill.PatientID
    myTitle.TitleID = MyPatient.TitleID
    myLab.LabID = 1
    myStaff.StaffID = MyBill.UserID
    
    csetPrinter.SetPrinterAsDefault PrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = PrinterName Then
            Set Printer = myP
        End If
    Next
    
    If SelectForm(PaperName, formHdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Tahoma"
    
    Printer.FontSize = 15
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Name)) / 2)
    Printer.Print DecreptedWord(myLab.Name)

    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.RegNo)) / 2)
    Printer.Print DecreptedWord(myLab.RegNo)

    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Address)) / 2)
    Printer.Print DecreptedWord(myLab.Address)
    
    Printer.FontSize = 12
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Telephone1)) / 2)
    Printer.Print DecreptedWord(myLab.Telephone1)
    
    Printer.FontSize = 11
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Email)) / 2)
    Printer.Print DecreptedWord(myLab.Email)
    
    
    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2)
    Printer.Print "Laboratory Receipt Cancellation"

    Dim displacement As Integer
    
    displacement = 0
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 11
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & myTitle.Title & " " & MyPatient.Name
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.Time, "hh:mm AMPM")) + 740)
    Printer.Print Format(MyBill.Time, "hh:mm AMPM")
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.PatientIxBillID
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.billDate, "dd MMM yyyy")) + 740)
    Printer.Print Format(MyBill.billDate, "dd MMM yyyy")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 50 + displacement
    Tab4 = 36 + displacement
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSql = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.IxDisplay, 38); Tab(Tab3); Right((Space(10)) & Format(myPtIx.Value, "0.00"), 10)
            Printer.Print
            .MoveNext
        Wend
        .Close
    End With
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 40 + displacement
    Tab4 = 46 + displacement
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Value, "0.00")), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetValue)), 10)
        End If
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        .EndDoc
    End With
End Sub



Public Sub printLabBillDouble(BillID As Long, PrinterName As String, PaperName As String, formHdc)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyBill As New PatientIxBill
    Dim MyPatient As New Patient
    Dim myPtIx As New PatientIx
    Dim myBillItem As New PatientIxItem
    Dim MyIx As New Ix
    Dim myTitle As New Title
    Dim myStaff As New Staff
    Dim myLab As New LabDetails
    
    MyBill.PatientIxBillID = BillID
    MyPatient.PatientID = MyBill.PatientID
    myTitle.TitleID = MyPatient.TitleID
    myLab.LabID = 1
    myStaff.StaffID = MyBill.UserID
    
    csetPrinter.SetPrinterAsDefault PrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = PrinterName Then
            Set Printer = myP
        End If
    Next
    
    If SelectForm(PaperName, formHdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Tahoma"
    
    Printer.FontSize = 15
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Name)) / 2)
    Printer.Print DecreptedWord(myLab.Name)

    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.RegNo)) / 2)
    Printer.Print DecreptedWord(myLab.RegNo)

    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Address)) / 2)
    Printer.Print DecreptedWord(myLab.Address)
    
    Printer.FontSize = 12
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Telephone1)) / 2)
    Printer.Print DecreptedWord(myLab.Telephone1)
    
    Printer.FontSize = 11
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Email)) / 2)
    Printer.Print DecreptedWord(myLab.Email)
    
    
    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2)
    Printer.Print "Laboratory Receipt"

    Dim displacement As Integer
    
    displacement = -6
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 11
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & myTitle.Title & " " & MyPatient.Name
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.Time, "hh:mm AMPM")) + 740)
    Printer.Print Format(MyBill.Time, "hh:mm AMPM")
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.PatientIxBillID
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.billDate, "dd MMM yyyy")) + 740)
    Printer.Print Format(MyBill.billDate, "dd MMM yyyy")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 50 + displacement
    Tab4 = 36 + displacement
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSql = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.IxDisplay, 38); Tab(Tab3); Right((Space(10)) & Format(myPtIx.Value, "0.00"), 10)
            Printer.Print
            .MoveNext
        Wend
        .Close
    End With
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 40 + displacement
    Tab4 = 46 + displacement
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Value, "0.00")), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetValue)), 10)
        End If
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        
        Printer.Print
        Printer.Print
        
        Printer.Font = "Tahoma"
    
    Printer.FontSize = 15
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Name)) / 2)
    Printer.Print DecreptedWord(myLab.Name)

    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.RegNo)) / 2)
    Printer.Print DecreptedWord(myLab.RegNo)

    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Address)) / 2)
    Printer.Print DecreptedWord(myLab.Address)
    
    Printer.FontSize = 12
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Telephone1)) / 2)
    Printer.Print DecreptedWord(myLab.Telephone1)
    
    Printer.FontSize = 11
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Email)) / 2)
    Printer.Print DecreptedWord(myLab.Email)
    
    
    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2)
    Printer.Print "Laboratory Receipt"

    
    displacement = -6
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    

    Printer.FontSize = 11
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & myTitle.Title & " " & MyPatient.Name
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.Time, "hh:mm AMPM")) + 740)
    Printer.Print Format(MyBill.Time, "hh:mm AMPM")
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.PatientIxBillID
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.billDate, "dd MMM yyyy")) + 740)
    Printer.Print Format(MyBill.billDate, "dd MMM yyyy")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 50 + displacement
    Tab4 = 36 + displacement
    
    End With
    
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    With rsTem
        temSql = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.IxDisplay, 38); Tab(Tab3); Right((Space(10)) & Format(myPtIx.Value, "0.00"), 10)
            Printer.Print
            .MoveNext
        Wend
        .Close
    End With
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 40 + displacement
    Tab4 = 46 + displacement
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Value, "0.00")), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetValue)), 10)
        End If
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        
        
        
        
        .EndDoc
    End With
    
    
End Sub




Public Sub printLabBillImpact(BillID As Long, PrinterName As String, PaperName As String, formHdc)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyBill As New PatientIxBill
    Dim MyPatient As New Patient
    Dim myPtIx As New PatientIx
    Dim myBillItem As New PatientIxItem
    Dim MyIx As New Ix
    Dim myTitle As New Title
    Dim myStaff As New Staff
    Dim myLab As New LabDetails
    
    MyBill.PatientIxBillID = BillID
    MyPatient.PatientID = MyBill.PatientID
    myTitle.TitleID = MyPatient.TitleID
    myLab.LabID = 1
    myStaff.StaffID = MyBill.UserID
    
    csetPrinter.SetPrinterAsDefault PrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = PrinterName Then
            Set Printer = myP
        End If
    Next
    
    If SelectForm(PaperName, formHdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Tahoma"
    
    Printer.FontSize = 15
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Name)) / 2)
    Printer.Print DecreptedWord(myLab.Name)

    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.RegNo)) / 2)
    Printer.Print DecreptedWord(myLab.RegNo)

    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Address)) / 2)
    Printer.Print DecreptedWord(myLab.Address)
    
    Printer.FontSize = 12
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Telephone1)) / 2)
    Printer.Print DecreptedWord(myLab.Telephone1)
    
    Printer.FontSize = 11
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Email)) / 2)
    Printer.Print DecreptedWord(myLab.Email)
    
    
    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2)
    Printer.Print "Laboratory Receipt"
    
    Dim displacement As Integer
    displacement = 5
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 11
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & myTitle.Title & " " & MyPatient.Name
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.Time, "hh:mm AMPM")) + 740)
    Printer.Print Format(MyBill.Time, "hh:mm AMPM")
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.PatientIxBillID
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.billDate, "dd MMM yyyy")) + 740)
    Printer.Print Format(MyBill.billDate, "dd MMM yyyy")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 64 + displacement
    Tab4 = 36 + displacement
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSql = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.IxDisplay, 38); Tab(Tab3); Right((Space(10)) & Format(myPtIx.Value, "0.00"), 10)
            Printer.Print
            .MoveNext
        Wend
        .Close
    End With
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 64 + displacement
    Tab4 = 46 + displacement
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Value, "0.00")), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetValue)), 10)
        End If
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        .EndDoc
    End With
End Sub

